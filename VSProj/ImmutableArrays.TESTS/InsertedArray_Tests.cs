﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace TcKs.ImmutableArrays.TESTS
{
    public class InsertedArray_Tests
    {
		[Fact]
		public void CanInsertOneNullIntoNull() {
			var arrNull0 = ImArray.Null<int>();
			var arrNull1 = ImArray.Null<int>();

			var result = arrNull0.InsertOne(0, arrNull1);
			Assert.True(result.IsNull);
			Assert.True(result.IsEmpty);
			Assert.True(result.IsNullOrEmpty);
			Assert.Equal(0, result.Length);
		}

		[Fact]
		public void CanInsertOneNullIntoEmpty() {
			var arrNull = ImArray.Null<int>();
			var arrEmpty = ImArray.Empty<int>();

			var result = arrEmpty.InsertOne(0, arrNull);
			Assert.False(result.IsNull);
			Assert.True(result.IsEmpty);
			Assert.True(result.IsNullOrEmpty);
			Assert.Equal(0, result.Length);
		}

		[Fact]
		public void CanInsertOneEmptyIntoNull() {
			var arrNull = ImArray.Null<int>();
			var arrEmpty = ImArray.Empty<int>();

			var result = arrNull.InsertOne(0, arrEmpty);
			Assert.False(result.IsNull);
			Assert.True(result.IsEmpty);
			Assert.True(result.IsNullOrEmpty);
			Assert.Equal(0, result.Length);
		}

		[Fact]
		public void CanInsertOneEmptyIntoEmpty() {
			var arrEmpty0 = ImArray.Empty<int>();
			var arrEmpty1 = ImArray.Empty<int>();

			var result = arrEmpty0.InsertOne(0, arrEmpty1);
			Assert.False(result.IsNull);
			Assert.True(result.IsEmpty);
			Assert.True(result.IsNullOrEmpty);
			Assert.Equal(0, result.Length);
		}

		[Fact]
		public void CanInsertOneLen1IntoLen1() {
			var arrLen1_A = ImArray.SingleElement("A");
			var arrLen1_B = ImArray.SingleElement("B");

			{
				var result = arrLen1_A.InsertOne(0, arrLen1_B);
				Assert.Equal(2, result.Length);
				Assert.Equal("B", result[0]);
				Assert.Equal("A", result[1]);
			}

			{
				var result = arrLen1_A.InsertOne(1, arrLen1_B);
				Assert.Equal(2, result.Length);
				Assert.Equal("A", result[0]);
				Assert.Equal("B", result[1]);
			}
		}

		[Fact]
		public void CanInsertOneLen2IntoLen2() {
			var arrLen2_AB = ImArray.Create(new[] { "A", "B" });
			var arrLen2_CD = ImArray.Create(new[] { "C", "D" });

			{
				var result = arrLen2_AB.InsertOne(0, arrLen2_CD);
				Assert.Equal(4, result.Length);
				Assert.Equal("C", result[0]);
				Assert.Equal("D", result[1]);
				Assert.Equal("A", result[2]);
				Assert.Equal("B", result[3]);
			}

			{
				var result = arrLen2_AB.InsertOne(1, arrLen2_CD);
				Assert.Equal(4, result.Length);
				Assert.Equal("A", result[0]);
				Assert.Equal("C", result[1]);
				Assert.Equal("D", result[2]);
				Assert.Equal("B", result[3]);
			}

			{
				var result = arrLen2_AB.InsertOne(2, arrLen2_CD);
				Assert.Equal(4, result.Length);
				Assert.Equal("A", result[0]);
				Assert.Equal("B", result[1]);
				Assert.Equal("C", result[2]);
				Assert.Equal("D", result[3]);
			}
		}

		[Fact]
		public void CanInsertNullsIntoNull() {
			var arrNull0 = ImArray.Null<int>();
			var arrNull1 = ImArray.Null<int>();
			var arrNull2 = ImArray.Null<int>();

			var result = arrNull0.Insert(0, new [] { arrNull1, arrNull2 });
			Assert.True(result.IsNull);
			Assert.True(result.IsEmpty);
			Assert.True(result.IsNullOrEmpty);
		}

		[Fact]
		public void CanInsertNullsIntoEmpty() {
			var arrNull0 = ImArray.Null<int>();
			var arrNull1 = ImArray.Null<int>();
			var arrNull2 = ImArray.Null<int>();
			var arrEmpty = ImArray.Empty<int>();

			var result = arrEmpty.Insert(0, new[] { arrNull1, arrNull2 });
			Assert.False(result.IsNull);
			Assert.True(result.IsEmpty);
			Assert.True(result.IsNullOrEmpty);
		}

		[Fact]
		public void CanInsertNullsEmptysIntoNull() {
			var arrNull0 = ImArray.Null<int>();
			var arrNull1 = ImArray.Null<int>();
			var arrNull2 = ImArray.Null<int>();
			var arrEmpty0 = ImArray.Empty<int>();
			var arrEmpty1 = ImArray.Empty<int>();

			var result = arrNull0.Insert(0, new[] { arrNull1, arrEmpty0, arrNull2, arrEmpty1 });
			Assert.False(result.IsNull);
			Assert.True(result.IsEmpty);
			Assert.True(result.IsNullOrEmpty);
		}

		[Fact]
		public void CanInsertNullsEmptysIntoEmpty() {
			var arrNull0 = ImArray.Null<int>();
			var arrNull1 = ImArray.Null<int>();
			var arrNull2 = ImArray.Null<int>();
			var arrEmpty0 = ImArray.Empty<int>();
			var arrEmpty1 = ImArray.Empty<int>();
			var arrEmpty2 = ImArray.Empty<int>();

			var result = arrEmpty2.Insert(0, new[] { arrNull1, arrEmpty0, arrNull2, arrEmpty1 });
			Assert.False(result.IsNull);
			Assert.True(result.IsEmpty);
			Assert.True(result.IsNullOrEmpty);
		}

		[Fact]
		public void CanInsertLen1sIntoNull() {
			var arrNull = ImArray.Null<string>();
			var arrLen1_A = ImArray.SingleElement("A");
			var arrLen1_B = ImArray.SingleElement("B");

			var result = arrNull.Insert(0, arrLen1_A, arrLen1_B);
			Assert.False(result.IsNull);
			Assert.False(result.IsEmpty);
			Assert.Equal(2, result.Length);
			Assert.Equal("A", result[0]);
			Assert.Equal("B", result[1]);
		}

		[Fact]
		public void CanInsertLen1sIntoEmpty() {
			var arrEmpty = ImArray.Empty<string>();
			var arrLen1_A = ImArray.SingleElement("A");
			var arrLen1_B = ImArray.SingleElement("B");

			var result = arrEmpty.Insert(0, arrLen1_A, arrLen1_B);
			Assert.False(result.IsNull);
			Assert.False(result.IsEmpty);
			Assert.Equal(2, result.Length);
			Assert.Equal("A", result[0]);
			Assert.Equal("B", result[1]);
		}

		[Fact]
		public void CanInsertLen1sIntoLen1() {
			var arrLen1_A = ImArray.SingleElement("A");
			var arrLen1_B = ImArray.SingleElement("B");
			var arrLen1_C = ImArray.SingleElement("C");

			{
				var result = arrLen1_A.Insert(0, arrLen1_B, arrLen1_C);
				Assert.False(result.IsNull);
				Assert.False(result.IsEmpty);
				Assert.Equal(3, result.Length);
				Assert.Equal("B", result[0]);
				Assert.Equal("C", result[1]);
				Assert.Equal("A", result[2]);
			}

			{
				var result = arrLen1_A.Insert(1, arrLen1_B, arrLen1_C);
				Assert.False(result.IsNull);
				Assert.False(result.IsEmpty);
				Assert.Equal(3, result.Length);
				Assert.Equal("A", result[0]);
				Assert.Equal("B", result[1]);
				Assert.Equal("C", result[2]);
			}
		}

		[Fact]
		public void CanInsertLen2sIntoLen2() {
			var arrLen2_AB = ImArray.Create(new[] { "A", "B" });
			var arrLen2_CD = ImArray.Create(new[] { "C", "D" });
			var arrLen3_EF = ImArray.Create(new[] { "E", "F" });

			{
				var result = arrLen2_AB.Insert(0, arrLen2_CD, arrLen3_EF);
				Assert.False(result.IsNull);
				Assert.False(result.IsEmpty);
				Assert.Equal(6, result.Length);
				Assert.Equal("C", result[0]);
				Assert.Equal("D", result[1]);
				Assert.Equal("E", result[2]);
				Assert.Equal("F", result[3]);
				Assert.Equal("A", result[4]);
				Assert.Equal("B", result[5]);
			}

			{
				var result = arrLen2_AB.Insert(1, arrLen2_CD, arrLen3_EF);
				Assert.False(result.IsNull);
				Assert.False(result.IsEmpty);
				Assert.Equal(6, result.Length);
				Assert.Equal("A", result[0]);
				Assert.Equal("C", result[1]);
				Assert.Equal("D", result[2]);
				Assert.Equal("E", result[3]);
				Assert.Equal("F", result[4]);
				Assert.Equal("B", result[5]);
			}

			{
				var result = arrLen2_AB.Insert(2, arrLen2_CD, arrLen3_EF);
				Assert.False(result.IsNull);
				Assert.False(result.IsEmpty);
				Assert.Equal(6, result.Length);
				Assert.Equal("A", result[0]);
				Assert.Equal("B", result[1]);
				Assert.Equal("C", result[2]);
				Assert.Equal("D", result[3]);
				Assert.Equal("E", result[4]);
				Assert.Equal("F", result[5]);
			}

			{
				Assert.Throws<ArgumentOutOfRangeException>(() => arrLen2_AB.Insert(3, arrLen2_CD, arrLen3_EF));
			}
		}
	}
}
