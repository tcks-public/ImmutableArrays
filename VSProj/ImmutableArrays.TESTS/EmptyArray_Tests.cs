/* Licenced under MIT. Complete license is in LINCENSE.txt in root. */

using System;
using System.Collections.Generic;
using Xunit;

namespace TcKs.ImmutableArrays.TESTS
{
	public sealed class EmptyArray_Tests {
		[Fact]
		public void EqualsWorks() {
			var arrNull = ImArray.Null<object>();
			var arrEmpty1 = ImArray.Empty<object>();
			var arrEmpty2 = ImArray.Empty<object>();
			var arrLen0 = new SimpleArray<object>(new object[0]);
			var arrLen1 = new SimpleArray<object>(new object[1]);

			Assert.False(arrEmpty1.Equals(null));
			Assert.False(arrEmpty1.Equals(arrNull));

			Assert.True(arrEmpty1.Equals(arrEmpty1));
			Assert.True(arrEmpty1.Equals(arrEmpty1, EqualityComparer<object>.Default));
			Assert.True(arrEmpty1.Equals(arrEmpty2));
			Assert.True(arrEmpty1.Equals(arrEmpty2, EqualityComparer<object>.Default));
			Assert.True(arrEmpty2.Equals(arrEmpty1));
			Assert.True(arrEmpty2.Equals(arrEmpty1, EqualityComparer<object>.Default));

			Assert.True(arrEmpty1.Equals(arrLen0));
			Assert.True(arrEmpty1.Equals(arrLen0, EqualityComparer<object>.Default));
			Assert.True(arrEmpty2.Equals(arrLen0));
			Assert.True(arrEmpty2.Equals(arrLen0, EqualityComparer<object>.Default));

			Assert.False(arrEmpty1.Equals(arrLen1));
			Assert.False(arrEmpty1.Equals(arrLen1, EqualityComparer<object>.Default));
			Assert.False(arrEmpty2.Equals(arrLen1));
			Assert.False(arrEmpty2.Equals(arrLen1, EqualityComparer<object>.Default));
		}

		[Fact]
		public void StructuralCompareWorks() {
			var arrEmpty1 = ImArray.Empty<object>();
			var arrEmpty2 = ImArray.Empty<object>();

			Assert.Equal(0, arrEmpty1.CompareTo(arrEmpty1, Comparer<object>.Default));
			Assert.Equal(0, arrEmpty1.CompareTo(arrEmpty2, Comparer<object>.Default));
			Assert.Equal(0, arrEmpty2.CompareTo(arrEmpty1, Comparer<object>.Default));

			var arrNull1 = ImArray.Null<object>();
			Assert.Equal(0, arrEmpty1.CompareTo(arrNull1, Comparer<object>.Default));

			var arrLen0 = new SimpleArray<object>(new object[0]);
			Assert.Equal(0, arrEmpty1.CompareTo(arrLen0, Comparer<object>.Default));
		}

		[Fact]
		public void CanBeUsedInForeachLoop() {
			var arr = ImArray.Empty<object>();
			Assert.Equal(false, arr.IsComposed);
			Assert.Equal(0, arr.Length);
			Assert.True(arr.IsEmpty);
			Assert.False(arr.IsNull);
			Assert.True(arr.IsNullOrEmpty);

			foreach (var element in arr) {
				throw new InvalidOperationException("The empty array should not provide any element.");
			}
		}
	}
}
