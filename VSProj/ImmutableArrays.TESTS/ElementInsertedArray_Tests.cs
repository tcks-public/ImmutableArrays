﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace TcKs.ImmutableArrays.TESTS {
	public class ElementInsertedArray_Tests {
		[Fact]
		public void CanInsertToNull_Test() {
			var result = ImArray.Null<int>().Insert(0, 5);
			Assert.False(result.IsNull);
			Assert.False(result.IsEmpty);
			Assert.Equal(1, result.Length);
			Assert.Equal(5, result[0]);
		}

		[Fact]
		public void CanInsertToEmpty_Test() {
			var result = ImArray.Empty<int>().Insert(0, 5);
			Assert.False(result.IsNull);
			Assert.False(result.IsEmpty);
			Assert.Equal(1, result.Length);
			Assert.Equal(5, result[0]);
		}

		[Fact]
		public void CanInsertToLen0_Test() {
			var result = new SimpleArray<int>(new int[0]).Insert(0, 5);
			Assert.False(result.IsNull);
			Assert.False(result.IsEmpty);
			Assert.Equal(1, result.Length);
			Assert.Equal(5, result[0]);
		}

		[Fact]
		public void CanInsertToLen5_Test() {
			{
				var result = new SimpleArray<int>(new int[] { 1, 2, 3, 4, 5 }).Insert(0, 7);
				Assert.False(result.IsNull);
				Assert.False(result.IsEmpty);
				Assert.Equal(6, result.Length);
				Assert.Equal(7, result[0]);
				Assert.Equal(1, result[1]);
				Assert.Equal(2, result[2]);
				Assert.Equal(3, result[3]);
				Assert.Equal(4, result[4]);
				Assert.Equal(5, result[5]);
			}

			{
				var result = new SimpleArray<int>(new int[] { 1, 2, 3, 4, 5 }).Insert(5, 7);
				Assert.False(result.IsNull);
				Assert.False(result.IsEmpty);
				Assert.Equal(6, result.Length);
				Assert.Equal(1, result[0]);
				Assert.Equal(2, result[1]);
				Assert.Equal(3, result[2]);
				Assert.Equal(4, result[3]);
				Assert.Equal(5, result[4]);
				Assert.Equal(7, result[5]);
			}

			{
				var result = new SimpleArray<int>(new int[] { 1, 2, 3, 4, 5 }).Insert(3, 7);
				Assert.False(result.IsNull);
				Assert.False(result.IsEmpty);
				Assert.Equal(6, result.Length);
				Assert.Equal(1, result[0]);
				Assert.Equal(2, result[1]);
				Assert.Equal(3, result[2]);
				Assert.Equal(7, result[3]);
				Assert.Equal(4, result[4]);
				Assert.Equal(5, result[5]);
			}
		}
	}
}
