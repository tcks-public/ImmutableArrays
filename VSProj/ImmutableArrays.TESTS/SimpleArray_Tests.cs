﻿/* Licenced under MIT. Complete license is in LINCENSE.txt in root. */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace TcKs.ImmutableArrays.TESTS
{
	public class SimpleArray_Tests {
		[Fact]
		public void EqualsWorks() {
			var arrNull = ImArray.Null<object>();
			var arrEmpty = ImArray.Empty<object>();
			var arrLen0 = new SimpleArray<object>(new object[0]);
			var arrLen0B = new SimpleArray<object>(new object[0]);
			var arrLen1_3 = new SimpleArray<object>(new object[1] { 3 });
			var arrLen1_3B = new SimpleArray<object>(new object[1] { 3 });
			var arrLen1_5 = new SimpleArray<object>(new object[1] { 5 });
			var arrLen1_5B = new SimpleArray<object>(new object[1] { 5 });

			Assert.False(arrLen0.Equals(null));
			Assert.False(arrLen0.Equals(null, EqualityComparer<object>.Default));
			Assert.False(arrLen1_3.Equals(null));
			Assert.False(arrLen1_3.Equals(null, EqualityComparer<object>.Default));

			Assert.False(arrLen0.Equals(arrNull));
			Assert.False(arrLen0.Equals(arrNull, EqualityComparer<object>.Default));
			Assert.False(arrLen1_3.Equals(arrNull));
			Assert.False(arrLen1_3.Equals(arrNull, EqualityComparer<object>.Default));

			Assert.True(arrLen0.Equals(arrEmpty));
			Assert.True(arrLen0.Equals(arrEmpty, EqualityComparer<object>.Default));
			Assert.True(arrLen0.Equals(arrLen0B));
			Assert.True(arrLen0.Equals(arrLen0B, EqualityComparer<object>.Default));
			Assert.True(arrLen0B.Equals(arrLen0));
			Assert.True(arrLen0B.Equals(arrLen0, EqualityComparer<object>.Default));

			Assert.False(arrLen1_3.Equals(arrEmpty));
			Assert.False(arrLen1_3.Equals(arrEmpty, EqualityComparer<object>.Default));
			Assert.False(arrLen1_3.Equals(arrLen1_5));
			Assert.False(arrLen1_3.Equals(arrLen1_5, EqualityComparer<object>.Default));

			Assert.True(arrLen1_3.Equals(arrLen1_3));
			Assert.True(arrLen1_3.Equals(arrLen1_3, EqualityComparer<object>.Default));
			Assert.True(arrLen1_3B.Equals(arrLen1_3B));
			Assert.True(arrLen1_3B.Equals(arrLen1_3B, EqualityComparer<object>.Default));

			Assert.True(arrLen1_3.Equals(arrLen1_3B));
			Assert.True(arrLen1_3.Equals(arrLen1_3B, EqualityComparer<object>.Default));
			Assert.True(arrLen1_3B.Equals(arrLen1_3));
			Assert.True(arrLen1_3B.Equals(arrLen1_3, EqualityComparer<object>.Default));

			Assert.False(arrLen1_3.Equals(arrLen1_5));
			Assert.False(arrLen1_3.Equals(arrLen1_5, EqualityComparer<object>.Default));
			Assert.False(arrLen1_5.Equals(arrLen1_3));
			Assert.False(arrLen1_5.Equals(arrLen1_3, EqualityComparer<object>.Default));

			Assert.True(arrLen1_5.Equals(arrLen1_5));
			Assert.True(arrLen1_5.Equals(arrLen1_5, EqualityComparer<object>.Default));
			Assert.True(arrLen1_5B.Equals(arrLen1_5B));
			Assert.True(arrLen1_5B.Equals(arrLen1_5B, EqualityComparer<object>.Default));

			Assert.True(arrLen1_5.Equals(arrLen1_5B));
			Assert.True(arrLen1_5.Equals(arrLen1_5B, EqualityComparer<object>.Default));
			Assert.True(arrLen1_5B.Equals(arrLen1_5));
			Assert.True(arrLen1_5B.Equals(arrLen1_5, EqualityComparer<object>.Default));
		}

		[Fact]
		public void StructuralCompareWorks() {
			var arrNull = ImArray.Null<object>();
			var arrEmpty = ImArray.Empty<object>();
			var arrLen0 = new SimpleArray<object>(new object[0]);
			var arrLen0B = new SimpleArray<object>(new object[0]);
			var arrLen1_3 = ImArray.Create(new object[1] { 3 });
			var arrLen1_3B = ImArray.Create(new object[1] { 3 });
			var arrLen1_5 = ImArray.Create(new object[1] { 5 });
			var arrLen1_5B = ImArray.Create(new object[1] { 5 });
			var arrLen2_1_3 = ImArray.Create(new object[2] { 1, 3 });
			var arrLen2_1_3B = ImArray.Create(new object[2] { 1, 3 });
			var arrLen2_1_5 = ImArray.Create(new object[2] { 1, 5 });
			var arrLen2_1_5B = ImArray.Create(new object[2] { 1, 5 });
			var arrLen2_3_2 = ImArray.Create(new object[2] { 3, 2 });
			var arrLen2_3_2B = ImArray.Create(new object[2] { 3, 2 });

			Assert.Equal(0, arrLen0.CompareTo(arrNull, Comparer<object>.Default));
			Assert.Equal(0, arrLen0.CompareTo(arrEmpty, Comparer<object>.Default));
			Assert.Equal(0, arrLen0.CompareTo(arrLen0, Comparer<object>.Default));
			Assert.Equal(0, arrLen0.CompareTo(arrLen0B, Comparer<object>.Default));

			Assert.Throws<ArgumentException>(() => arrLen0.CompareTo(arrLen1_3, Comparer<object>.Default));
			Assert.Throws<ArgumentException>(() => arrLen0.CompareTo(arrLen1_5, Comparer<object>.Default));
			Assert.Throws<ArgumentException>(() => arrLen0B.CompareTo(arrLen1_3, Comparer<object>.Default));
			Assert.Throws<ArgumentException>(() => arrLen0B.CompareTo(arrLen1_5, Comparer<object>.Default));

			Assert.Equal(0, arrLen1_3.CompareTo(arrLen1_3, Comparer<object>.Default));
			Assert.Equal(0, arrLen1_3.CompareTo(arrLen1_3B, Comparer<object>.Default));
			Assert.Equal(0, arrLen1_3B.CompareTo(arrLen1_3, Comparer<object>.Default));
			Assert.Equal(0, arrLen1_3B.CompareTo(arrLen1_3B, Comparer<object>.Default));
			Assert.Equal(-1, arrLen1_3.CompareTo(arrLen1_5, Comparer<object>.Default));
			Assert.Equal(-1, arrLen1_3.CompareTo(arrLen1_5B, Comparer<object>.Default));
			Assert.Equal(-1, arrLen1_3B.CompareTo(arrLen1_5, Comparer<object>.Default));
			Assert.Equal(-1, arrLen1_3B.CompareTo(arrLen1_5B, Comparer<object>.Default));
			Assert.Equal(1, arrLen1_5.CompareTo(arrLen1_3, Comparer<object>.Default));
			Assert.Equal(1, arrLen1_5.CompareTo(arrLen1_3B, Comparer<object>.Default));
			Assert.Equal(1, arrLen1_5B.CompareTo(arrLen1_3, Comparer<object>.Default));
			Assert.Equal(1, arrLen1_5B.CompareTo(arrLen1_3B, Comparer<object>.Default));

			Assert.Throws<ArgumentException>(() => arrLen1_3.CompareTo(arrNull, Comparer<object>.Default));
			Assert.Throws<ArgumentException>(() => arrLen1_3.CompareTo(arrEmpty, Comparer<object>.Default));
			Assert.Throws<ArgumentException>(() => arrLen1_3.CompareTo(arrLen0, Comparer<object>.Default));
			Assert.Throws<ArgumentException>(() => arrLen1_3.CompareTo(arrLen2_1_3, Comparer<object>.Default));
			Assert.Throws<ArgumentException>(() => arrLen1_3.CompareTo(arrLen2_1_5, Comparer<object>.Default));
			Assert.Throws<ArgumentException>(() => arrLen1_3.CompareTo(arrLen2_3_2, Comparer<object>.Default));

			Assert.Equal(0, arrLen1_5.CompareTo(arrLen1_5, Comparer<object>.Default));
			Assert.Equal(0, arrLen1_5.CompareTo(arrLen1_5B, Comparer<object>.Default));
			Assert.Equal(0, arrLen1_5B.CompareTo(arrLen1_5, Comparer<object>.Default));
			Assert.Equal(0, arrLen1_5B.CompareTo(arrLen1_5B, Comparer<object>.Default));

			Assert.Throws<ArgumentException>(() => arrLen1_5.CompareTo(arrNull, Comparer<object>.Default));
			Assert.Throws<ArgumentException>(() => arrLen1_5.CompareTo(arrEmpty, Comparer<object>.Default));
			Assert.Throws<ArgumentException>(() => arrLen1_5.CompareTo(arrLen0, Comparer<object>.Default));
			Assert.Throws<ArgumentException>(() => arrLen1_5.CompareTo(arrLen2_1_3, Comparer<object>.Default));
			Assert.Throws<ArgumentException>(() => arrLen1_5.CompareTo(arrLen2_1_5, Comparer<object>.Default));
			Assert.Throws<ArgumentException>(() => arrLen1_5.CompareTo(arrLen2_3_2, Comparer<object>.Default));

			Assert.Equal(0, arrLen2_1_3.CompareTo(arrLen2_1_3, Comparer<object>.Default));
			Assert.Equal(0, arrLen2_1_3.CompareTo(arrLen2_1_3B, Comparer<object>.Default));
			Assert.Equal(0, arrLen2_1_3B.CompareTo(arrLen2_1_3, Comparer<object>.Default));
			Assert.Equal(0, arrLen2_1_3B.CompareTo(arrLen2_1_3B, Comparer<object>.Default));
			Assert.Equal(-1, arrLen2_1_3.CompareTo(arrLen2_1_5, Comparer<object>.Default));
			Assert.Equal(-1, arrLen2_1_3.CompareTo(arrLen2_1_5B, Comparer<object>.Default));
			Assert.Equal(-1, arrLen2_1_3B.CompareTo(arrLen2_1_5, Comparer<object>.Default));
			Assert.Equal(-1, arrLen2_1_3B.CompareTo(arrLen2_1_5B, Comparer<object>.Default));
			Assert.Equal(1, arrLen2_1_5.CompareTo(arrLen2_1_3, Comparer<object>.Default));
			Assert.Equal(1, arrLen2_1_5.CompareTo(arrLen2_1_3B, Comparer<object>.Default));
			Assert.Equal(1, arrLen2_1_5B.CompareTo(arrLen2_1_3, Comparer<object>.Default));
			Assert.Equal(1, arrLen2_1_5B.CompareTo(arrLen2_1_3B, Comparer<object>.Default));
			Assert.Equal(-1, arrLen2_1_3.CompareTo(arrLen2_3_2, Comparer<object>.Default));
			Assert.Equal(-1, arrLen2_1_3.CompareTo(arrLen2_3_2B, Comparer<object>.Default));
			Assert.Equal(-1, arrLen2_1_3B.CompareTo(arrLen2_3_2, Comparer<object>.Default));
			Assert.Equal(-1, arrLen2_1_3B.CompareTo(arrLen2_3_2B, Comparer<object>.Default));

			Assert.Throws<ArgumentException>(() => arrLen2_1_3.CompareTo(arrNull, Comparer<object>.Default));
			Assert.Throws<ArgumentException>(() => arrLen2_1_3.CompareTo(arrEmpty, Comparer<object>.Default));
			Assert.Throws<ArgumentException>(() => arrLen2_1_3.CompareTo(arrLen0, Comparer<object>.Default));
			Assert.Throws<ArgumentException>(() => arrLen2_1_3.CompareTo(arrLen1_3, Comparer<object>.Default));
			Assert.Throws<ArgumentException>(() => arrLen2_1_3.CompareTo(arrLen1_5, Comparer<object>.Default));

			Assert.Equal(0, arrLen2_1_5.CompareTo(arrLen2_1_5, Comparer<object>.Default));
			Assert.Equal(0, arrLen2_1_5.CompareTo(arrLen2_1_5B, Comparer<object>.Default));
			Assert.Equal(0, arrLen2_1_5B.CompareTo(arrLen2_1_5, Comparer<object>.Default));
			Assert.Equal(0, arrLen2_1_5B.CompareTo(arrLen2_1_5B, Comparer<object>.Default));
			Assert.Equal(1, arrLen2_1_5.CompareTo(arrLen2_1_3, Comparer<object>.Default));
			Assert.Equal(1, arrLen2_1_5.CompareTo(arrLen2_1_3B, Comparer<object>.Default));
			Assert.Equal(1, arrLen2_1_5B.CompareTo(arrLen2_1_3, Comparer<object>.Default));
			Assert.Equal(1, arrLen2_1_5B.CompareTo(arrLen2_1_3B, Comparer<object>.Default));
			Assert.Equal(-1, arrLen2_1_3.CompareTo(arrLen2_3_2, Comparer<object>.Default));
			Assert.Equal(-1, arrLen2_1_3.CompareTo(arrLen2_3_2B, Comparer<object>.Default));
			Assert.Equal(-1, arrLen2_1_3B.CompareTo(arrLen2_3_2, Comparer<object>.Default));
			Assert.Equal(-1, arrLen2_1_3B.CompareTo(arrLen2_3_2B, Comparer<object>.Default));

			Assert.Throws<ArgumentException>(() => arrLen2_1_5.CompareTo(arrNull, Comparer<object>.Default));
			Assert.Throws<ArgumentException>(() => arrLen2_1_5.CompareTo(arrEmpty, Comparer<object>.Default));
			Assert.Throws<ArgumentException>(() => arrLen2_1_5.CompareTo(arrLen0, Comparer<object>.Default));
			Assert.Throws<ArgumentException>(() => arrLen2_1_5.CompareTo(arrLen1_3, Comparer<object>.Default));
			Assert.Throws<ArgumentException>(() => arrLen2_1_5.CompareTo(arrLen1_5, Comparer<object>.Default));

			Assert.Equal(0, arrLen2_3_2.CompareTo(arrLen2_3_2, Comparer<object>.Default));
			Assert.Equal(0, arrLen2_3_2.CompareTo(arrLen2_3_2B, Comparer<object>.Default));
			Assert.Equal(0, arrLen2_3_2B.CompareTo(arrLen2_3_2, Comparer<object>.Default));
			Assert.Equal(0, arrLen2_3_2B.CompareTo(arrLen2_3_2B, Comparer<object>.Default));
			Assert.Equal(1, arrLen2_3_2.CompareTo(arrLen2_1_5, Comparer<object>.Default));
			Assert.Equal(1, arrLen2_3_2.CompareTo(arrLen2_1_5B, Comparer<object>.Default));
			Assert.Equal(1, arrLen2_3_2B.CompareTo(arrLen2_1_5, Comparer<object>.Default));
			Assert.Equal(1, arrLen2_3_2B.CompareTo(arrLen2_1_5B, Comparer<object>.Default));
			Assert.Equal(1, arrLen2_3_2.CompareTo(arrLen2_1_3, Comparer<object>.Default));
			Assert.Equal(1, arrLen2_3_2.CompareTo(arrLen2_1_3B, Comparer<object>.Default));
			Assert.Equal(1, arrLen2_3_2B.CompareTo(arrLen2_1_3, Comparer<object>.Default));
			Assert.Equal(1, arrLen2_3_2B.CompareTo(arrLen2_1_3, Comparer<object>.Default));

			Assert.Throws<ArgumentException>(() => arrLen2_3_2.CompareTo(arrNull, Comparer<object>.Default));
			Assert.Throws<ArgumentException>(() => arrLen2_3_2.CompareTo(arrEmpty, Comparer<object>.Default));
			Assert.Throws<ArgumentException>(() => arrLen2_3_2.CompareTo(arrLen0, Comparer<object>.Default));
			Assert.Throws<ArgumentException>(() => arrLen2_3_2.CompareTo(arrLen1_3, Comparer<object>.Default));
			Assert.Throws<ArgumentException>(() => arrLen2_3_2.CompareTo(arrLen1_5, Comparer<object>.Default));
		}

		[Fact]
		public void CanBeUsedInForeachLoop() {
			var lengths = new int[] { 0, 1, 2, 3, 4, 5, 10, 100, 1000, 10_000 };

			foreach (var length in lengths) {
				var originalArray = Enumerable.Range(length, length).ToArray();
				var copiedArray = (int[])originalArray.Clone();
				var target = ImArray.Create(copiedArray);
				Assert.Equal(false, target.IsComposed);
				Assert.Equal(originalArray.Length, target.Length);

				var counter = 0;
				foreach (var number in target) {
					Assert.Equal(originalArray[counter], number);
					counter++;
				}

				Assert.Equal(originalArray.Length, counter);
			}
		}

		[Fact]
		public void CanBeUsedInForLoop() {
			var lengths = new int[] { 0, 1, 2, 3, 4, 5, 10, 100, 1000, 10_000 };

			foreach (var length in lengths) {
				var originalArray = Enumerable.Range(length, length).ToArray();
				var copiedArray = (int[])originalArray.Clone();
				var target = ImArray.Create(copiedArray);
				Assert.Equal(false, target.IsComposed);
				Assert.Equal(originalArray.Length, target.Length);

				var counter = 0;
				for (var i = 0; i < originalArray.Length; i++) {
					Assert.Equal(originalArray[i], target[i]);
					counter++;
				}

				Assert.Equal(originalArray.Length, counter);
			}
		}
	}
}