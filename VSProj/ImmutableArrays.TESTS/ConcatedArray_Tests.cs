﻿/* Licenced under MIT. Complete license is in LINCENSE.txt in root. */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

using TcKs.ImmutableArrays;

namespace TcKs.ImmutableArrays.TESTS {
	public class ConcatedArray_Tests {
		[Fact]
		public void CanBeUsedInForeachLoop() {
			var lengths = new int[] { 0, 1, 2, 3, 4, 5, 10, 100, 1000, 10_000 };

			foreach (var length_A in lengths) {
				var originalArray_A = Enumerable.Range(length_A, length_A).Cast<int?>().ToArray();
				var immutable_A = ImArray.Create(originalArray_A);

				foreach (var length_B in lengths) {
					var originalArray_B = Enumerable.Range(length_A, length_B).Cast<int?>().ToArray();
					var immutable_B = ImArray.Create(originalArray_B);

					var originalArray_Concated = originalArray_A.Concat(originalArray_B).ToArray();

					var target = ImArray.Concat(immutable_A, immutable_B);

					Assert.Equal(originalArray_Concated.Length, target.Length);

					var counter = 0;
					foreach (var number in target) {
						Assert.Equal(originalArray_Concated[counter], number);
						counter++;
					}

					Assert.Equal(originalArray_Concated.Length, counter);
				}
			}
		}

		[Fact]
		public void CanBeUsedInForLoop() {
			var lengths = new int[] { 0, 1, 2, 3, 4, 5, 10, 100, 1000, 10_000 };

			foreach (var length_A in lengths) {
				var originalArray_A = Enumerable.Range(length_A, length_A).ToArray();
				var immutable_A = ImArray.Create(originalArray_A);

				foreach (var length_B in lengths) {
					var originalArray_B = Enumerable.Range(length_A, length_B).ToArray();
					var immutable_B = ImArray.Create(originalArray_B);

					var originalArray_Concated = originalArray_A.Concat(originalArray_B).ToArray();

					var target = ImArray.Concat(immutable_A, immutable_B);

					Assert.Equal(originalArray_Concated.Length, target.Length);

					var counter = 0;
					for (var i = 0; i < originalArray_Concated.Length; i++) {
						Assert.Equal(originalArray_Concated[i], target[i]);
						counter++;
					}

					Assert.Equal(originalArray_Concated.Length, counter);
				}
			}
		}

		[Fact]
		public void CanConcat_1000_Arrays() {
			var origArrays = Enumerable.Range(1, 1000).Select(x => Enumerable.Range(x, x)).Select(x => ImArray.Create(x)).ToArray();
			var allItems = (from arr in origArrays
							from x in arr
							select x).ToArray();

			var swatchConcat = System.Diagnostics.Stopwatch.StartNew();
			var finalArray = ImArray.Concat(origArrays);
			swatchConcat.Stop();

			var swatchCheck = System.Diagnostics.Stopwatch.StartNew();
			for (var i = 0; i < allItems.Length; i++) {
				Assert.Equal(allItems[i], finalArray[i]);
			}
			swatchCheck.Stop();

			return;
		}

		[Fact]
		public void CanConcatConcatedArrays() {
			var arr_A = ImArray.Create(new int[] { 1, 2, 3 });
			var arr_B = ImArray.Create(new int[] { 4, 5, 6, 7, 8, 9 });
			var arr_C = ImArray.Create(new int[] { 10, 11, 12, 13, 14 });
			var arr_D = ImArray.Create(new int[] { 15, 16, 17, 18, 19 });

			var arr_ABCD_1 = ImArray.Concat(arr_A, arr_B, arr_C, arr_D);

			var arr_AB = ImArray.Concat(arr_A, arr_B);
			var arr_CD = ImArray.Concat(arr_C, arr_D);
			var arr_ABCD_2 = ImArray.Concat(arr_AB, arr_CD);

			for (var i = 0; i < arr_ABCD_1.Length; i++) {
				Assert.Equal(arr_ABCD_1[i], arr_ABCD_2[i]);
			}
		}

		[Fact]
		public void WorksWithDifferentCountOfConcatedArrays() {
			var lengths = new int[] { 0, 1, 2, 3, 4, 5, 10, 100, 1000 };
			var sourceArrays = lengths.Select(x => Enumerable.Range(x, x).ToArray()).ToArray();
			var imArrays = sourceArrays.Select(x => ImArray.Create(x)).ToList();

			long counter = 0;
			var swatch = System.Diagnostics.Stopwatch.StartNew();
			var combinations = Variate(imArrays, 1, 5);
			foreach (var combination in combinations) {
				counter++;

				var arrCombination = combination.ToArray();

				var allItems = (from arr in arrCombination
								from x in arr
								select x).ToArray();
				var expectedEmpty = (allItems.Length < 1);

				var combinedArray = ImArray.Concat(arrCombination);
				Assert.False(combinedArray.IsNull);
				Assert.Equal(expectedEmpty, combinedArray.IsEmpty);
				Assert.Equal(allItems.Length, combinedArray.Length);

				for (var i = 0; i < allItems.Length; i++) {
					Assert.Equal(allItems[i], combinedArray[i]);
				}

				continue;
			}
			swatch.Stop();

			return;
		}

		[Fact]
		public void CanConcatTwoArrays() {
			ImArray<int> null0 = null;
			ImArray<int> null1 = null;

			var arrNull0 = ImArray.Null<int>();
			var arrNull1 = ImArray.Null<int>();

			{
				var result = ImArray.Concat(null0, null1);
				Assert.True(result.IsNull);
			}

			{
				var result = ImArray.Concat(null0, arrNull0);
				Assert.True(result.IsNull);
			}

			{
				var result = ImArray.Concat(arrNull0, null0);
				Assert.True(result.IsNull);
			}

			{
				var result = ImArray.Concat(arrNull0, arrNull1);
				Assert.True(result.IsNull);
			}

			///////////////////////

			{
				var result = null0.ConcatOne<int>(null1);
				Assert.True(result.IsNull);
			}

			{
				var result = null0.ConcatOne(arrNull0);
				Assert.True(result.IsNull);
			}

			{
				var result = arrNull0.ConcatOne(null0);
				Assert.True(result.IsNull);
			}

			{
				var result = arrNull0.ConcatOne(arrNull1);
				Assert.True(result.IsNull);
			}
		}

		[Fact]
		public void ConcatedArrayFromNullArraysIsNull() {
			var nullArrays = Enumerable.Range(0, 10).Select(n => ImArray.Null<object>()).ToArray();

			for (var i = 0; i < nullArrays.Length; i++) {
				var sourceArrays = nullArrays.Take(i + 1);
				var concatedArray = ImArray.Concat(sourceArrays);

				Assert.True(concatedArray.IsNull);
				Assert.True(concatedArray.IsEmpty);
				Assert.True(concatedArray.IsNullOrEmpty);
			}
		}

		private IEnumerable<IEnumerable<T>> Variate<T>(IList<T> sourceItems, int length) {
			return Variate<T>(sourceItems, length, length);
		}
		private IEnumerable<IEnumerable<T>> Variate<T>(IList<T> sourceItems, int minLength, int maxLength) {
			return Variate(sourceItems, minLength, maxLength, 1);
		}
		private IEnumerable<IEnumerable<T>> Variate<T>(IList<T> sourceItems, int minLength, int maxLength, int currentLength) {
			foreach (var item in sourceItems) {
				var arrItem = ImArray.SingleElement<T>(item);
				if (minLength <= currentLength) { yield return arrItem; }
				if (currentLength >= maxLength) { continue; }

				foreach (var variation in Variate<T>(sourceItems, minLength, maxLength, currentLength + 1)) {
					yield return arrItem.Concat(variation).ToArray();
				}
			}
		}
	}
}
