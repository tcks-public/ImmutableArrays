﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace TcKs.ImmutableArrays.TESTS {
	public class ReversedArray_Tests {
		[Fact]
		public void CanReverseNullArray() {
			var arrNull = ImArray.Null<int>();

			{
				var result = new ReversedArray<int>(arrNull);
				Assert.True(result.IsNull);
				Assert.True(result.IsEmpty);
				Assert.Equal(0, result.Length);
				Assert.True(result.IsComposed);
			}

			{
				var result = ImArray.Reverse(arrNull);
				Assert.True(result.IsNull);
				Assert.True(result.IsEmpty);
				Assert.Equal(0, result.Length);
				Assert.True(result.IsComposed);
			}

			{
				var result = arrNull.Reverse();
				Assert.True(result.IsNull);
				Assert.True(result.IsEmpty);
				Assert.Equal(0, result.Length);
				Assert.True(result.IsComposed);
			}
		}

		[Fact]
		public void CanReverseEmptyArray() {
			var arrEmpty = ImArray.Empty<int>();

			{
				var result = new ReversedArray<int>(arrEmpty);
				Assert.False(result.IsNull);
				Assert.True(result.IsEmpty);
				Assert.Equal(0, result.Length);
				Assert.True(result.IsComposed);
			}

			{
				var result = ImArray.Reverse(arrEmpty);
				Assert.False(result.IsNull);
				Assert.True(result.IsEmpty);
				Assert.Equal(0, result.Length);
				Assert.True(result.IsComposed);
			}

			{
				var result = arrEmpty.Reverse();
				Assert.False(result.IsNull);
				Assert.True(result.IsEmpty);
				Assert.Equal(0, result.Length);
				Assert.True(result.IsComposed);
			}
		}

		[Fact]
		public void CanReverseLen0Array() {
			var arrLen0 = new SimpleArray<int>(new int[0]);

			{
				var result = new ReversedArray<int>(arrLen0);
				Assert.False(result.IsNull);
				Assert.True(result.IsEmpty);
				Assert.Equal(0, result.Length);
				Assert.True(result.IsComposed);
			}

			{
				var result = ImArray.Reverse(arrLen0);
				Assert.False(result.IsNull);
				Assert.True(result.IsEmpty);
				Assert.Equal(0, result.Length);
				Assert.True(result.IsComposed);
			}

			{
				var result = arrLen0.Reverse();
				Assert.False(result.IsNull);
				Assert.True(result.IsEmpty);
				Assert.Equal(0, result.Length);
				Assert.True(result.IsComposed);
			}
		}

		[Fact]
		public void CanReverseLen1Array() {
			var arrLen1 = ImArray.SingleElement(13);

			{
				var result = new ReversedArray<int>(arrLen1);
				Assert.False(result.IsNull);
				Assert.False(result.IsEmpty);
				Assert.True(result.IsComposed);
				Assert.Equal(1, result.Length);
				Assert.Equal(13, result[0]);
			}

			{
				var result = ImArray.Reverse(arrLen1);
				Assert.False(result.IsNull);
				Assert.False(result.IsEmpty);
				Assert.True(result.IsComposed);
				Assert.Equal(1, result.Length);
				Assert.Equal(13, result[0]);
			}

			{
				var result = arrLen1.Reverse();
				Assert.False(result.IsNull);
				Assert.False(result.IsEmpty);
				Assert.True(result.IsComposed);
				Assert.Equal(1, result.Length);
				Assert.Equal(13, result[0]);
			}
		}

		[Fact]
		public void CanReverseLen2Array() {
			var arrLen2 = ImArray.Create(new int[] { 21, 13 });

			{
				var result = new ReversedArray<int>(arrLen2);
				Assert.False(result.IsNull);
				Assert.False(result.IsEmpty);
				Assert.True(result.IsComposed);
				Assert.Equal(2, result.Length);
				Assert.Equal(13, result[0]);
				Assert.Equal(21, result[1]);
			}

			{
				var result = ImArray.Reverse(arrLen2);
				Assert.False(result.IsNull);
				Assert.False(result.IsEmpty);
				Assert.True(result.IsComposed);
				Assert.Equal(2, result.Length);
				Assert.Equal(13, result[0]);
				Assert.Equal(21, result[1]);
			}

			{
				var result = arrLen2.Reverse();
				Assert.False(result.IsNull);
				Assert.False(result.IsEmpty);
				Assert.True(result.IsComposed);
				Assert.Equal(2, result.Length);
				Assert.Equal(13, result[0]);
				Assert.Equal(21, result[1]);
			}
		}

		[Fact]
		public void CanReverseDifferentArrays() {
			var sourceArrays = new int[][] {
				new int[0], new int[]{ 5 }, new int[]{12,13 }, new int[]{1,2,3,4,5,6,7,9,10 },
				Enumerable.Range(5,20).ToArray(), Enumerable.Range(56,10000).ToArray()
			};

			IEnumerable<ImArray<T>> CreateReversedArrays<T>(ImArray<T> sourceArray)
			{
				yield return new ReversedArray<T>(sourceArray);
				yield return ImArray.Reverse(sourceArray);
				yield return sourceArray.Reverse();
			}

			foreach (var sourceArray in sourceArrays) {
				var origArray = ImArray.Create(sourceArray);
				foreach (var reversedArray in CreateReversedArrays(origArray)) {
					Assert.Equal(origArray.IsNull, reversedArray.IsNull);
					Assert.Equal(origArray.IsEmpty, reversedArray.IsEmpty);
					Assert.Equal(origArray.IsNullOrEmpty, reversedArray.IsNullOrEmpty);
					Assert.Equal(origArray.Length, reversedArray.Length);
					Assert.True(reversedArray.IsComposed);

					for (var i = 0; i < origArray.Length; i++) {
						var x = origArray.Length - 1 - i;
						Assert.Equal(origArray[i], reversedArray[x]);
					}
				}
			}
		}
	}
}