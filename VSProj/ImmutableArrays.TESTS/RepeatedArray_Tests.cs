﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace TcKs.ImmutableArrays.TESTS {
	public class RepeatedArray_Tests {
		[Fact]
		public void CanRepeatNullArray() {
			var arrNull = ImArray.Null<int>();

			var multipliers = Enumerable.Range(1, 10).ToArray();
			foreach (var multiplier in multipliers) {
				var result = arrNull.Repeat(multiplier);
				Assert.True(result.IsNull);
				Assert.True(result.IsEmpty);
				Assert.True(result.IsNullOrEmpty);
				Assert.Equal(0, result.Length);
			}
		}

		[Fact]
		public void CanRepeatEmptyArray() {
			var arrEmpty = ImArray.Empty<int>();

			var multipliers = Enumerable.Range(1, 10).ToArray();
			foreach (var multiplier in multipliers) {
				var result = arrEmpty.Repeat(multiplier);
				Assert.False(result.IsNull);
				Assert.True(result.IsEmpty);
				Assert.True(result.IsNullOrEmpty);
				Assert.Equal(0, result.Length);
			}
		}

		[Fact]
		public void CanRepeatNonEmptyArrays() {
			var sourceArrays = Enumerable.Range(1, 20).Select(x => Enumerable.Range(x, x).ToArray()).ToArray();
			var origArrays = sourceArrays.Select(x => ImArray.Create(x)).ToArray();

			var multipliers = Enumerable.Range(1, 10).ToArray();

			foreach (var origArray in origArrays) {
				foreach (var multiplier in multipliers) {
					var result = origArray.Repeat(multiplier);
					Assert.Equal(origArray.IsNull, result.IsNull);
					Assert.Equal(origArray.IsEmpty, result.IsEmpty);
					Assert.Equal(origArray.Length * multiplier, result.Length);

					for (var i = 0; i < origArray.Length; i++) {
						var origItem = origArray[i];
						Assert.Equal(origItem, result[i]);

						for (var m = 0; m < multiplier; m++) {
							Assert.Equal(origItem, result[i + (m * origArray.Length)]);
						}
					}
				}
			}
		}

		[Fact]
		public void CanNotRepeatArraysByZero() {
			var sourceArrays = Enumerable.Range(0, 20).Select(x => Enumerable.Range(x, x).ToArray()).ToArray();
			var origArrays = sourceArrays.Select(x => ImArray.Create(x)).ToArray();


			foreach (var origArray in origArrays) {
				Assert.Throws<ArgumentOutOfRangeException>(() => origArray.Repeat(0));
			}
		}
	}
}
