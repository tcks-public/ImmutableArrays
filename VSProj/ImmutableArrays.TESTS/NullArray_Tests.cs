/* Licenced under MIT. Complete license is in LINCENSE.txt in root. */

using System;
using System.Collections.Generic;
using Xunit;

namespace TcKs.ImmutableArrays.TESTS {
	public sealed class NullArray_Tests {
		[Fact]
		public void EqualsWorks() {
			var arrNull1 = ImArray.Null<object>();
			
			Assert.True(arrNull1.Equals(null));
			Assert.True(arrNull1.Equals(null, EqualityComparer<object>.Default));

			Assert.True(arrNull1.Equals(arrNull1));
			Assert.True(object.Equals(arrNull1, arrNull1));

			Assert.True(arrNull1.Equals(arrNull1, EqualityComparer<object>.Default));

			var arrNull2 = ImArray.Null<object>();
			Assert.False(object.ReferenceEquals(arrNull1, arrNull2));

			Assert.True(arrNull1.Equals(arrNull2));
			Assert.True(object.Equals(arrNull1, arrNull2));

			Assert.True(arrNull1.Equals(arrNull2, EqualityComparer<object>.Default));
			Assert.True(arrNull2.Equals(arrNull1, EqualityComparer<object>.Default));

			var arrNull3 = ImArray.Null<int>();
			Assert.False(arrNull1.Equals(arrNull3));
			Assert.False(arrNull3.Equals(arrNull1));

			var arrEmpty = ImArray.Empty<object>();
			Assert.False(arrNull1.Equals(arrEmpty));
		}

		[Fact]
		public void StructuralCompareWorks() {
			var arrNull1 = ImArray.Null<object>();
			var arrNull2 = ImArray.Null<object>();

			Assert.Equal(0, arrNull1.CompareTo(arrNull1, Comparer<object>.Default));
			Assert.Equal(0, arrNull1.CompareTo(arrNull2, Comparer<object>.Default));
			Assert.Equal(0, arrNull2.CompareTo(arrNull1, Comparer<object>.Default));

			var arrEmpty1 = ImArray.Empty<object>();
			Assert.Equal(0, arrNull1.CompareTo(arrEmpty1, Comparer<object>.Default));

			var arrLen0 = new SimpleArray<object>(new object[0]);
			Assert.Equal(0, arrNull1.CompareTo(arrLen0, Comparer<object>.Default));
		}

		[Fact]
		public void CanBeUsedInForeachLoop() {
			var arr = ImArray.Null<object>();
			Assert.Equal(false, arr.IsComposed);
			Assert.Equal(0, arr.Length);
			Assert.True(arr.IsEmpty);
			Assert.True(arr.IsNull);
			Assert.True(arr.IsNullOrEmpty);

			foreach (var element in arr) {
				throw new InvalidOperationException("The empty array should not provide any element.");
			}
		}
	}
}
