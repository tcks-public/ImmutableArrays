﻿/* Licenced under MIT. Complete license is in LINCENSE.txt in root. */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace TcKs.ImmutableArrays.TESTS {
	public class RangeArray_Tests {
		[Fact]
		public void CanRangeNullArray() {
			var origArray = ImArray.Null<object>();

			var startIndeces = new int[] { 0, 1, 2, 3, 4, 5, 10, 100, 1000, 10_000 };
			var lengths = new int[] { 0, 1, 2, 3, 4, 5, 10, 100, 1000, 10_000 };

			foreach (var startIndex in startIndeces) {
				{
					var subset = ImArray.Range(origArray, startIndex);
					Assert.Equal(0, subset.Length);
					Assert.True(subset.IsEmpty);
					Assert.True(subset.IsNullOrEmpty);
					Assert.True(subset.IsNull);
				}

				foreach (var length in lengths) {
					{
						var subset = ImArray.Range(origArray, startIndex, length);
						Assert.Equal(0, subset.Length);
						Assert.True(subset.IsEmpty);
						Assert.True(subset.IsNullOrEmpty);
						Assert.True(subset.IsNull);
					}
				}
			}
		}

		[Fact]
		public void CanRangeEmptyArray() {
			var origArray = ImArray.Empty<object>();

			var startIndeces = new int[] { 0, 1, 2, 3, 4, 5, 10, 100, 1000, 10_000 };
			var lengths = new int[] { 0, 1, 2, 3, 4, 5, 10, 100, 1000, 10_000 };

			foreach (var startIndex in startIndeces) {
				{
					var subset = ImArray.Range(origArray, startIndex);
					Assert.Equal(0, subset.Length);
					Assert.True(subset.IsEmpty);
					Assert.True(subset.IsNullOrEmpty);
					Assert.False(subset.IsNull);
				}

				foreach (var length in lengths) {
					{
						var subset = ImArray.Range(origArray, startIndex, length);
						Assert.Equal(0, subset.Length);
						Assert.True(subset.IsEmpty);
						Assert.True(subset.IsNullOrEmpty);
						Assert.False(subset.IsNull);
					}
				}
			}
		}

		[Fact]
		public void RangeArrayIsCorrect() {
			var sourceArrays = new int[][] {
				new int[0], new int[]{13}, new int[]{1, 2}, new int[] {1, 2, 3}, new int[] {1, 2, 3, 4},
				new int[] {1, 2, 3, 4, 5}, new int[] {1, 2, 3, 4, 5, 6}, new int[] {1, 2, 3, 4, 5, 7},
				Enumerable.Range(1,10).ToArray(), Enumerable.Range(1,100).ToArray(),
				Enumerable.Range(1,1000).ToArray(), Enumerable.Range(1,10_000).ToArray(),
				Enumerable.Range(1,100_000).ToArray(), Enumerable.Range(1,1000_000).ToArray()
			};

			foreach (var sourceArray in sourceArrays) {
				var origArray = ImArray.Create<int>(sourceArray);

				var startIndeces = new int[] { 0, 1, 2, 3, 4, 5, 10, 100, 1000, 10_000 };
				var lengths = new int[] { 0, 1, 2, 3, 4, 5, 10, 100, 1000, 10_000 };

				foreach (var startIndex in startIndeces) {
					{
						var subset = ImArray.Range(origArray, startIndex);

						var expectedLength = Math.Max(0, origArray.Length - startIndex);
						Assert.Equal(expectedLength, subset.Length);

						var expectedIsEmpty = expectedLength < 1;
						Assert.Equal(expectedIsEmpty, subset.IsEmpty);
						Assert.Equal(expectedIsEmpty, subset.IsNullOrEmpty);
						Assert.False(subset.IsNull);

						for (var i = startIndex; i < origArray.Length; i++) {
							Assert.Equal(origArray[i], subset[i - startIndex]);
						}
					}

					foreach (var length in lengths) {
						{
							var subset = ImArray.Range(origArray, startIndex, length);

							var expectedLength = Math.Max(0, Math.Min(length, origArray.Length - startIndex));
							Assert.Equal(expectedLength, subset.Length);

							var expectedIsEmpty = expectedLength < 1;
							Assert.Equal(expectedIsEmpty, subset.IsEmpty);
							Assert.Equal(expectedIsEmpty, subset.IsNullOrEmpty);
							Assert.False(subset.IsNull);

							for (var i = startIndex; i < origArray.Length && (i - startIndex) < length; i++) {
								Assert.Equal(origArray[i], subset[i - startIndex]);
							}
						}
					}
				}
			}
		}
	}
}
