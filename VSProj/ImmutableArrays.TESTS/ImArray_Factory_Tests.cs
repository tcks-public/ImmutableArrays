﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace TcKs.ImmutableArrays.TESTS {
	public sealed class ImArray_Factory_Tests {
		[Fact]
		public void NullFactoryWorks() {
			var arrNull1 = ImArray.Null<object>();
			var arrNull2 = ImArray.Null<object>();

			Assert.NotSame(arrNull1, arrNull2);
			Assert.Equal(typeof(NullArray<object>), arrNull1.GetType());
			Assert.Equal(typeof(NullArray<object>), arrNull2.GetType());
		}

		[Fact]
		public void EmptyFactoryWorks() {
			var arrEmpty1 = ImArray.Empty<object>();
			var arrEmpty2 = ImArray.Empty<object>();

			Assert.NotSame(arrEmpty1, arrEmpty2);
			Assert.Equal(typeof(EmptyArray<object>), arrEmpty1.GetType());
			Assert.Equal(typeof(EmptyArray<object>), arrEmpty2.GetType());
		}

		[Fact]
		public void SingleElementFactoryWorks() {
			var arr_0 = ImArray.SingleElement(5);
			Assert.Equal(typeof(SingleElementArray<int>), arr_0.GetType());

			var arr_1 = ImArray.SingleElement<object>(null);
			Assert.Equal(typeof(SingleElementArray<object>), arr_1.GetType());
		}

		[Fact]
		public void CreateFactoryWorks_With_Null() {
			var arrNull1 = ImArray.Create<object>(null);
			var arrNull2 = ImArray.Create<object>(null);

			Assert.NotSame(arrNull1, arrNull2);
			Assert.Equal(typeof(NullArray<object>), arrNull1.GetType());
			Assert.Equal(typeof(NullArray<object>), arrNull2.GetType());
		}

		[Fact]
		public void CreateFactoryWorks_With_Empty() {
			var arr1 = new object[0];
			var arr2 = new object[0];

			var arrEmpty1 = ImArray.Create<object>(arr1);
			var arrEmpty2 = ImArray.Create<object>(arr1);
			var arrEmpty3 = ImArray.Create<object>(arr2);
			var arrEmpty4 = ImArray.Create<object>(arr2);

			Assert.NotSame(arrEmpty1, arrEmpty2);
			Assert.NotSame(arrEmpty1, arrEmpty3);
			Assert.NotSame(arrEmpty1, arrEmpty4);
			Assert.NotSame(arrEmpty2, arrEmpty3);
			Assert.NotSame(arrEmpty2, arrEmpty4);

			Assert.Equal(typeof(EmptyArray<object>), arrEmpty1.GetType());
			Assert.Equal(typeof(EmptyArray<object>), arrEmpty2.GetType());
			Assert.Equal(typeof(EmptyArray<object>), arrEmpty3.GetType());
			Assert.Equal(typeof(EmptyArray<object>), arrEmpty3.GetType());
		}

		[Fact]
		public void CreateFactoryWorks_With_NonEmpty() {
			var arr1_1 = new object[1] { 1 };
			var arr2_5_7 = new object[2] { 5, 7 };
			var arr3_7_8_9 = new object[3] { 7, 8, 9 };

			var arrLen1_1 = ImArray.Create<object>(arr1_1);
			var arrLen1_1B = ImArray.Create<object>(arr1_1);

			var arrLen2_5_7 = ImArray.Create<object>(arr2_5_7);
			var arrLen2_5_7B = ImArray.Create<object>(arr2_5_7);

			var arrLen3_7_8_9 = ImArray.Create<object>(arr3_7_8_9);
			var arrLen3_7_8_9B = ImArray.Create<object>(arr3_7_8_9);

			Assert.NotSame(arrLen1_1, arrLen1_1B);
			Assert.NotSame(arrLen1_1, arrLen2_5_7);
			Assert.NotSame(arrLen1_1, arrLen2_5_7B);
			Assert.NotSame(arrLen1_1, arrLen3_7_8_9);
			Assert.NotSame(arrLen1_1, arrLen3_7_8_9B);
			Assert.NotSame(arrLen1_1B, arrLen2_5_7);
			Assert.NotSame(arrLen1_1B, arrLen2_5_7B);
			Assert.NotSame(arrLen1_1B, arrLen3_7_8_9);
			Assert.NotSame(arrLen1_1B, arrLen3_7_8_9B);
			Assert.NotSame(arrLen2_5_7, arrLen2_5_7B);
			Assert.NotSame(arrLen2_5_7, arrLen3_7_8_9);
			Assert.NotSame(arrLen2_5_7, arrLen3_7_8_9B);
			Assert.NotSame(arrLen3_7_8_9, arrLen3_7_8_9B);

			Assert.Equal(typeof(SingleElementArray<object>), arrLen1_1.GetType());
			Assert.Equal(typeof(SingleElementArray<object>), arrLen1_1B.GetType());
			Assert.Equal(typeof(SimpleArray<object>), arrLen2_5_7.GetType());
			Assert.Equal(typeof(SimpleArray<object>), arrLen2_5_7B.GetType());
			Assert.Equal(typeof(SimpleArray<object>), arrLen3_7_8_9.GetType());
			Assert.Equal(typeof(SimpleArray<object>), arrLen3_7_8_9B.GetType());
		}

		[Fact]
		public void ConcatFactoryWorks_With_OnlyNulls() {
			var arrNull = ImArray.Null<object>();
			var arrNullB = ImArray.Null<object>();

			var components = new ImArray<object>[] {
				null, arrNull, arrNullB
			};

			var pairs = (from c1 in components
						 from c2 in components
						 select new { c1, c2 });

			foreach (var pair in pairs) {
				var rslt = ImArray.Concat(pair.c1, pair.c2);
				Assert.NotNull(rslt);
				Assert.Equal(typeof(NullArray<object>), rslt.GetType());
			}

			var trinities = (from c1 in components
							 from c2 in components
							 from c3 in components
							 select new { c1, c2, c3 });

			foreach (var trinity in trinities) {
				var rslt = ImArray.Concat(trinity.c1, trinity.c2, trinity.c3);
				Assert.NotNull(rslt);
				Assert.Equal(typeof(NullArray<object>), rslt.GetType());
			}
		}

		[Fact]
		public void ConcatFactoryWorks_With_EmptiesOrLess() {
			var arrNull = ImArray.Null<object>();
			var arrNullB = ImArray.Null<object>();
			var arrEmpty = ImArray.Empty<object>();
			var arrEmptyB = ImArray.Empty<object>();

			var nullComponents = new ImArray<object>[] {
				null, arrNull, arrNullB
			};
			var emptyComponents = new ImArray<object>[]{
				arrEmpty, arrEmptyB
			};

			var pairs = (from c1 in nullComponents
						 from c2 in emptyComponents
						 select new { c1, c2 });
			pairs = pairs.Concat(pairs.Reverse());

			foreach (var pair in pairs) {
				var rslt = ImArray.Concat(pair.c1, pair.c2);
				Assert.NotNull(rslt);

				var onlyNulls = ((pair.c1 is null || pair.c1.IsNull) && (pair.c2 is null || pair.c2.IsNull));
				if (onlyNulls) {
					Assert.Equal(typeof(NullArray<object>), rslt.GetType());
				}
				else {
					Assert.Equal(typeof(EmptyArray<object>), rslt.GetType());
				}
			}

			var trinities_A = (from p in pairs
							   from c3 in emptyComponents
							   select new { p.c1, p.c2, c3 });
			var trinities_B = (from p in pairs
							   from c3 in nullComponents
							   select new { p.c1, p.c2, c3 });
			var trinities = trinities_A.Concat(trinities_B);

			foreach (var trinity in trinities) {
				var rslt = ImArray.Concat(trinity.c1, trinity.c2, trinity.c3);
				Assert.NotNull(rslt);

				var onlyNulls = ((trinity.c1 is null || trinity.c1.IsNull)
								&& (trinity.c2 is null || trinity.c2.IsNull)
								&& (trinity.c3 is null || trinity.c3.IsNull));
				if (onlyNulls) {
					Assert.Equal(typeof(NullArray<object>), rslt.GetType());
				}
				else {
					Assert.Equal(typeof(EmptyArray<object>), rslt.GetType());
				}
			}
		}

		[Fact]
		public void ConcatFactoryWorks_With_Len0OrLess() {
			var arrNull = ImArray.Null<object>();
			var arrNullB = ImArray.Null<object>();
			var arrEmpty = ImArray.Empty<object>();
			var arrEmptyB = ImArray.Empty<object>();
			var arrLen0 = new SimpleArray<object>(new object[0]);
			var arrLen0B = new SimpleArray<object>(new object[0]);

			var nullComponents = new ImArray<object>[] {
				null, arrNull, arrNullB
			};
			var emptyComponents = new ImArray<object>[]{
				arrEmpty, arrEmptyB
			};
			var len0Components = new ImArray<object>[] {
				arrLen0, arrLen0B
			};

			var pairs_Len0_Null_A = (from c1 in nullComponents
									 from c2 in len0Components
									 select new { c1, c2 });
			var pairs_Len0_Null_B = (from c1 in len0Components
									 from c2 in nullComponents
									 select new { c1, c2 });
			var pairs_Len0_Empty_A = (from c1 in emptyComponents
									  from c2 in len0Components
									  select new { c1, c2 });
			var pairs_Len0_Empty_B = (from c1 in len0Components
									  from c2 in emptyComponents
									  select new { c1, c2 });
			var pairs_Len0_Len0 = (from c1 in len0Components
								   from c2 in len0Components
								   select new { c1, c2 });

			var allPairs = pairs_Len0_Null_A
							.Concat(pairs_Len0_Null_B)
							.Concat(pairs_Len0_Empty_A)
							.Concat(pairs_Len0_Empty_B)
							.Concat(pairs_Len0_Len0);

			foreach (var pair in allPairs) {
				var c1 = pair.c1;
				var c2 = pair.c2;

				var rslt = ImArray.Concat(c1, c2);
				Assert.NotNull(rslt);

				var expectedNull = (c1 is null || c1.IsNull) && (c2 is null || c2.IsNull);
				Assert.Equal(expectedNull, rslt.IsNull);

				var expectedEmpty = (c1 is null || c1.IsEmpty) && (c2 is null || c2.IsEmpty);
				Assert.Equal(expectedEmpty, rslt.IsEmpty);
			}

			var combinations = (from p1 in pairs_Len0_Null_A
								   from p2 in pairs_Len0_Null_B
								   from p3 in pairs_Len0_Empty_A
								   from p4 in pairs_Len0_Empty_B
								   from p5 in pairs_Len0_Len0
								   select new {
									   p1c1 = p1.c1,
									   p1c2 = p1.c2,
									   p2c1 = p2.c1,
									   p2c2 = p2.c2,
									   p3c1 = p3.c1,
									   p3c2 = p3.c2,
									   p4c1 = p4.c1,
									   p4c2 = p4.c2,
									   p5c1 = p5.c1,
									   p5c2 = p5.c2
								   });
			var moreCombinations = combinations.Concat(combinations.Reverse());

			foreach (var c in moreCombinations) {
				var rslt = ImArray.Concat(c.p1c1, c.p1c2, c.p2c1, c.p2c2, c.p3c1, c.p3c2, c.p4c1, c.p4c2, c.p5c1, c.p5c2);
				Assert.NotNull(rslt);
				Assert.Equal(typeof(EmptyArray<object>), rslt.GetType());
			}
		}
	}
}