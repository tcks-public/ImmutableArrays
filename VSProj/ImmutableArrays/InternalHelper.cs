﻿/* Licenced under MIT. Complete license is in LINCENSE.txt in root. */

using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Reflection;

namespace TcKs.ImmutableArrays {
	/// <summary>
	/// Helper class for internal purposes.
	/// </summary>
	public static class InternalHelper {
		/// <summary>
		/// Returns element at specified <paramref name="index"/> from array of arrays.
		/// </summary>
		/// <typeparam name="T">Type of elements in arrays.</typeparam>
		/// <param name="arrays">Array of arrays treated as one concated array.</param>
		/// <param name="index">Index of element.</param>
		/// <returns></returns>
		public static T GetElementAt<T>(ImArray<ImArray<T>> arrays, int index) {
			var skipedIndeces = 0;
			for (var a = 0; a < arrays.Length; a++) {
				var arr = arrays[a];
				if (arr is null) { continue; }

				var arrIndex = index - skipedIndeces;
				if (arrIndex < arr.Length) {
					return arr[arrIndex];
				}

				skipedIndeces += arr.Length;
			}

			throw new IndexOutOfRangeException();
		}

		/// <summary>
		/// Returns sum of lengths of arrays in <paramref name="arrays"/>.
		/// </summary>
		/// <typeparam name="T">Type of elements in arrays.</typeparam>
		/// <param name="arrays">The measured arrays.</param>
		/// <returns></returns>
		public static int GetLength<T>(ImArray<ImArray<T>> arrays) {
			var result = 0;

			var length = arrays.Length;
			for (var i = 0; i < length; i++) {
				var arr = arrays[i];
				if (arr is null) { continue; }

				result += arr.Length;
			}

			return result;
		}

		/// <summary>
		/// Returns <c>true</c> if <paramref name="obj"/> is null.
		/// Othervise returns <c>false</c>.
		/// </summary>
		/// <param name="obj">Object check for nullity.</param>
		/// <returns></returns>
		public static bool IsNotNull<T>(T obj) where T : class => !(obj is null);

		/// <summary>
		/// Returns true if <paramref name="arr"/> is null or if <see cref="ImArray.IsNull"/> is true.
		/// Othervise returns false.
		/// </summary>
		/// <typeparam name="T">Type of elements in array.</typeparam>
		/// <param name="arr">Checked array.</param>
		/// <returns></returns>
		public static bool IsNullArray<T>(ImArray<T> arr) => (arr is null || arr.IsNull);

		/// <summary>
		/// Returns false if <paramref name="arr"/> is null or if <see cref="ImArray.IsNull"/> is true.
		/// Othervise returns true.
		/// </summary>
		/// <typeparam name="T">Type of elements in array.</typeparam>
		/// <param name="arr">Checked array.</param>
		/// <returns></returns>
		public static bool IsNotNullArray<T>(ImArray<T> arr) => !(arr is null || arr.IsNull);

		/// <summary>
		/// Returns length of passed <paramref name="array"/>.
		/// </summary>
		/// <typeparam name="T">Type of elements in <paramref name="array"/>.</typeparam>
		/// <param name="array">Measured array. Can not be null.</param>
		/// <returns></returns>
		public static int GetLength<T>(ImArray<T> array) => array.Length;

		/// <summary>
		/// Wraps <paramref name="items"/> with lazy-evaluated enumerator.
		/// Good for wraping mutable object to something what is not mutable.
		/// </summary>
		/// <typeparam name="T">Type of elements in <paramref name="items"/>.</typeparam>
		/// <param name="items">Items to enumerate. Can not be null.</param>
		/// <returns></returns>
		public static IEnumerable<T> SimpleSelect<T>(IEnumerable<T> items) {
			foreach (var item in items) {
				yield return item;
			}
		}

		/// <summary>
		/// Returs <c>true</c> if passed <paramref name="array"/> is not <c>null</c> and is not <see cref="ImArray.IsNullOrEmpty"/> and <see cref="ImArray.RangeLeft{T}(ImArray{T}, int)"/> is greater than 0.
		/// Othervise returns <c>false</c>.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="array"></param>
		/// <returns></returns>
		public static bool HasAnyElements<T>(ImArray<T> array) => !(array is null) && !array.IsEmpty && !array.IsNull && 0 < array.Length;

		/// <summary>
		/// Combines two hash codes.
		/// </summary>
		/// <param name="a">First hash code.</param>
		/// <param name="b">Second has code.</param>
		/// <returns></returns>
		public static int CombineHashCodes(int a, int b)=> (((a << 5) + a) ^ b);

		/// <summary>
		/// Returns nice human readable name of <paramref name="type"/>.
		/// </summary>
		/// <param name="type">humanizing type.</param>
		/// <returns></returns>
		public static string HumanizeName(Type type) {
			var tpInfo = type.GetTypeInfo();
			if (tpInfo.IsGenericTypeDefinition) {
				var argNames = tpInfo.GenericTypeParameters.Select(x => HumanizeName(x)).ToArray();
				return HumanizeName(type, argNames);
			}
			else if (tpInfo.IsGenericType) {
				var tpDef = tpInfo.GetGenericTypeDefinition();
				var argNames = tpInfo.GenericTypeArguments.Select(x => HumanizeName(x)).ToArray();

				if (tpDef == typeof(Nullable<>)) {
					return $"{argNames[0]}?";
				}

				return HumanizeName(tpDef, argNames);
			}

			return type.Name;
		}

		private static string HumanizeName(Type type, string[] genericArgumentNames) {
			var baseName = type.Name;

			// removing generic-type suffix
			var ndx = baseName.LastIndexOf('`');
			if (ndx >= 0) { baseName = baseName.Substring(0, ndx); }

			return $"{baseName}<{string.Join(",", genericArgumentNames)}>";
		}
	}
}
