﻿/* Licenced under MIT. Complete license is in LINCENSE.txt in root. */

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TcKs.ImmutableArrays {
	/// <summary>
	/// Base class for all immutable arrays.
	/// Also contains factory methods for different kinds of immutable arrays.
	/// </summary>
	public abstract partial class 
		ImArray : IEnumerable {
		#region Nullity, emptiness & length members
		/// <summary>
		/// Returns length of this array.
		/// </summary>
		public abstract int Length { get; }

		/// <summary>
		/// Returns <c>true</c> if this array has no elements.
		/// Othervise returns <c>false</c>.
		/// 
		/// Null array is also empty.
		/// </summary>
		public virtual bool IsEmpty => this.Length < 1;

		/// <summary>
		/// Returns <c>true</c> if this array represents NULL value.
		/// Othervise returns <c>false</c>.
		/// 
		/// Empty not-null arrays are not NULL.
		/// </summary>
		public abstract bool IsNull { get; }

		/// <summary>
		/// Returns <c>true</c> if this array is <see cref="IsNull"/> or <see cref="IsEmpty"/>.
		/// Othervise returns <c>false</c>.
		/// </summary>
		public virtual bool IsNullOrEmpty => this.IsEmpty || this.IsNull;

		/// <summary>
		/// Returns type of containing elements.
		/// </summary>
		public abstract Type ElementType { get; }
		#endregion Nullity, emptiness & length members

		#region Composition members
		/// <summary>
		/// Returns <c>true</c> if is this array composed from another different array(s).
		/// Othervise return <c>false</c>.
		/// </summary>
		public abstract bool IsComposed { get; }

		/// <summary>
		/// Returns <c>true</c> if this array is not composed form another arrays but only from original pure array (<see cref="Array"/>).
		/// Othervise returns <c>false</c>.
		/// </summary>
		public virtual bool IsFlat => !this.IsComposed;
		#endregion Composition members

		#region Index members
		/// <summary>
		/// Returns element from specified <paramref name="index"/>.
		/// </summary>
		/// <param name="index">Index of element which will be returned.</param>
		/// <returns></returns>
		protected abstract object GetElementAt(int index);

		/// <summary>
		/// Returns element from specified <paramref name="index"/>.
		/// </summary>
		/// <param name="index">Index of element which will be returned.</param>
		/// <returns></returns>
		public object this[int index] => this.GetElementAt(index);
		#endregion Index members

		#region Enumeration members
		/// <summary>
		/// Implementation of <see cref="IEnumerable.GetEnumerator"/>.
		/// </summary>
		/// <returns></returns>
		protected abstract IEnumerator IEnumerator_GetEnumerator();

		IEnumerator IEnumerable.GetEnumerator() => this.IEnumerator_GetEnumerator();
		#endregion Enumeration members

		#region HashCode members
		private int? structuralHashCode;
		/// <summary>
		/// Returns hash code computed from containing items.
		/// </summary>
		/// <returns></returns>
		public int GetStructuralHashCode() {
			var result = this.structuralHashCode;
			if (!result.HasValue) {
				this.structuralHashCode = result = this.ComputeStructuralHashCode();
			}

			return result.Value;
		}

		private int ComputeStructuralHashCode() {
			var len = this.Length;
			if (len < 1) { return 0; }

			var result = this[0].GetHashCode();
			for (var i = 0; i < len; i++) {
				var code = this[i].GetHashCode();
				result = InternalHelper.CombineHashCodes(result, code);
			}

			return result;
		}
		#endregion HashCode members

		#region Stringification members
		private string toStringCache;
		/// <inheritdoc />
		public override string ToString() => toStringCache ?? (toStringCache = this.Stringify());

		/// <summary>
		/// Creates humand readable string description of this array.
		/// </summary>
		/// <returns></returns>
		public virtual string Stringify() {
			var elementType = this.ElementType ?? typeof(object);
			var elementTypeName = InternalHelper.HumanizeName(elementType);

			if (this.IsNull) {
				return $"!{elementTypeName}[NULL]!";
			}
			else {
				return $"!{elementTypeName}[{this.Length}]!";
			}
		}
		#endregion Stringification members

		#region Constructors
		/// <summary>
		/// Internal constructor.
		/// </summary>
		/// <remarks>
		/// Constructor is internal because we don't want to allow anyone else ihnerit from this class.
		/// </remarks>
		internal ImArray() { }
		#endregion Constructors
	}

	partial class ImArray {
		#region Factory methods
		/// <summary>
		/// Returns immutable array representing NULL value.
		/// </summary>
		/// <typeparam name="T">Type of elements in array.</typeparam>
		/// <returns></returns>
		public static ImArray<T> Null<T>() => new NullArray<T>();

		/// <summary>
		/// Returns empty immutable array.
		/// </summary>
		/// <typeparam name="T">Type of elements in array.</typeparam>
		/// <returns></returns>
		public static ImArray<T> Empty<T>() => new EmptyArray<T>();

		/// <summary>
		/// Creates immutable array with single element.
		/// </summary>
		/// <typeparam name="T">Type of elements in array.</typeparam>
		/// <param name="element">The element in array.</param>
		/// <returns></returns>
		public static ImArray<T> SingleElement<T>(T element) => new SingleElementArray<T>(element);

		/// <summary>
		/// Creates immutable array based on passed items.
		/// </summary>
		/// <remarks>
		/// If <paramref name="items"/> is <c>null</c>, the instance of <see cref="NullArray{T}"/> will be returned.
		/// if <paramref name="items"/> has no elements, the instance of <see cref="EmptyArray{T}"/> will be returned.
		/// Othervise the <see cref="SimpleArray{T}"/> will be returned.
		/// </remarks>
		/// <typeparam name="T">Type of elements in array.</typeparam>
		/// <param name="items">Items which should be contained in immutable array. Can be null. Can be empty. Can contains null elements.</param>
		/// <returns></returns>
		public static ImArray<T> Create<T>(IEnumerable<T> items) {
			if (items is null) {
				return Null<T>();
			}

			var arrItems = items.ToArray();
			if (arrItems.Length < 1) {
				return Empty<T>();
			}
			if (arrItems.Length < 2) {
				return new SingleElementArray<T>(arrItems[0]);
			}

			return new SimpleArray<T>(true, arrItems);
		}

		/// <summary>
		/// Concats immutable arrays in one array.
		/// </summary>
		/// <typeparam name="T">Type of elements in array.</typeparam>
		/// <param name="array1">First arrays to concat. Can be null. Can be empty. Elements can be null. Null or empty arrays will be skipped.</param>
		/// <param name="array2">First arrays to concat. Can be null. Can be empty. Elements can be null. Null or empty arrays will be skipped.</param>
		/// <returns></returns>
		public static ImArray<T> Concat<T>(ImArray<T> array1, ImArray<T> array2) {
			var isNull1 = array1 is null || array1.IsNull;
			var isNull2 = array2 is null || array2.IsNull;

			if (isNull1) {
				return isNull2 ? ImArray.Null<T>() : array2;
			}
			if (isNull2) {
				return isNull1 ? ImArray.Null<T>() : array1;
			}

			var sourceArrays = ImArray.Create(new ImArray<T>[] { array1, array2 });
			return new ConcatedArray<T>(default(InternalFlag), sourceArrays);
		}

		/// <summary>
		/// Concats immutable arrays in one array.
		/// </summary>
		/// <typeparam name="T">Type of elements in array.</typeparam>
		/// <param name="arrays">Arrays to concat. Can be null. Can be empty. Elements can be null. Null or empty arrays will be skipped.</param>
		/// <returns></returns>
		public static ImArray<T> Concat<T>(IEnumerable<ImArray<T>> arrays) {
			if (arrays is null) { return Null<T>(); }

			var hasNotNullArrays = false;
			var hasNotEmptyArrays = false;
			Func<ImArray<T>, ImArray<T>> checker = (arr) => {
				if (!(arr is null)) {
					if (!arr.IsEmpty) {
						hasNotNullArrays = true;
						hasNotEmptyArrays = true;
					}
					else if (!arr.IsNull) {
						hasNotNullArrays = true;
					}
				}

				return arr;
			};
			var finalArrays = arrays.Select(checker).Where(InternalHelper.HasAnyElements).ToArray();

			if (!hasNotNullArrays) { return Null<T>(); }
			if (!hasNotEmptyArrays) { return Empty<T>(); }

			var arrLength = finalArrays.Length;
			if (arrLength < 1) { return Empty<T>(); }
			if (arrLength < 2) { return finalArrays[0]; }

			return new ConcatedArray<T>(arrays);
		}

		/// <summary>
		/// Concats immutable arrays in one array.
		/// </summary>
		/// <typeparam name="T">Type of elements in array.</typeparam>
		/// <param name="firstArray">First array in concat operation. Can be null. Can be empty.</param>
		/// <param name="otherArrays">Other arrays in concat operation. Can be null. Can be empty. elements can be null.</param>
		/// <returns></returns>
		public static ImArray<T> Concat<T>(ImArray<T> firstArray, IEnumerable<ImArray<T>> otherArrays) {
			if (firstArray is null || firstArray.IsNullOrEmpty) {
				return Concat<T>(otherArrays);
			}
			else {
				var allArrays = new ImArray<T>[] { firstArray }.Concat(otherArrays);
				return Concat<T>(allArrays);
			}
		}

		/// <summary>
		/// Concats immutable arrays in one array.
		/// </summary>
		/// <typeparam name="T">Type of elements in array.</typeparam>
		/// <param name="arrays">Arrays to concat. Can be null. Can be empty. Null arrays will be skipped.</param>
		/// <returns></returns>
		public static ImArray<T> Concat<T>(params ImArray<T>[] arrays) => Concat<T>((IEnumerable<ImArray<T>>)arrays);

		/// <summary>
		/// Inserts <paramref name="array2"/> into <paramref name="array1"/> on specified <paramref name="index"/> in new immutable array.
		/// </summary>
		/// <typeparam name="T">Type of elements in arrays.</typeparam>
		/// <param name="array1">Base array.</param>
		/// <param name="index">Index where to insert.</param>
		/// <param name="array2">Inserting array.</param>
		/// <returns></returns>
		public static ImArray<T> InsertOne<T>(ImArray<T> array1, int index, ImArray<T> array2) {
			var isNull1 = (array1 is null || array1.IsNull);
			if (isNull1) {
				if (0 == index) {
					return array2 ?? ImArray.Null<T>();
				}

				throw new IndexOutOfRangeException();
			}

			var isNull2 = (array2 is null || array2.IsNull);
			if (array1.IsEmpty) {
				if (0 == index) {
					if (isNull2) {
						return array1;
					}
					else if (array2.IsEmpty) {
						return array1;
					}
					else {
						return array2;
					}
				}

				throw new IndexOutOfRangeException();
			}

			if (index < 0 || index > array1.Length) {
				throw new IndexOutOfRangeException();
			}

			var inserted = ImArray.SingleElement(array2);
			return new InsertedArray<T>(default(InternalFlag), array1, index, inserted);
		}

		/// <summary>
		/// Inserts <paramref name="arrays"/> into <paramref name="source"/> on specified <paramref name="index"/> in new immutable array.
		/// </summary>
		/// <typeparam name="T">Type of elements in arrays.</typeparam>
		/// <param name="source">Base array.</param>
		/// <param name="index">Index where to insert.</param>
		/// <param name="arrays">Inserting arrays.</param>
		/// <returns></returns>
		public static ImArray<T> Insert<T>(ImArray<T> source, int index, IEnumerable<ImArray<T>> arrays) {
			if (index < 0) { throw new ArgumentOutOfRangeException(nameof(index)); }

			var imArrays = arrays?.Where(InternalHelper.IsNotNullArray)?.ToImArray();
			if (imArrays is null || imArrays.IsNullOrEmpty) {
				return source ?? ImArray.Null<T>();
			}

			if (source is null || source.IsNullOrEmpty) {
				if (0 == index) {
					return Concat(arrays);
				}

				throw new ArgumentOutOfRangeException(nameof(index));
			}

			if (index > source.Length) {
				throw new ArgumentOutOfRangeException(nameof(index));
			}

			return new InsertedArray<T>(default(InternalFlag), source, index, imArrays);
		}

		/// <summary>
		/// Appends <paramref name="element"/> to the end of <paramref name="array"/> in new immutable array.
		/// </summary>
		/// <typeparam name="T">Type of elements in array.</typeparam>
		/// <param name="array">Base array.</param>
		/// <param name="element">Element to append.</param>
		/// <returns></returns>
		public static ImArray<T> Append<T>(ImArray<T> array, T element) {
			if (array is null || array.IsNullOrEmpty) {
				return SingleElement<T>(element);
			}

			return new ElementAppendedArray<T>(array, element);
		}

		/// <summary>
		/// Prepends <paramref name="element"/> to the end of <paramref name="array"/> in new immutable array.
		/// </summary>
		/// <typeparam name="T">Type of elements in array.</typeparam>
		/// <param name="array">Base array.</param>
		/// <param name="element">Element to prepend.</param>
		/// <returns></returns>
		public static ImArray<T> Prepend<T>(ImArray<T> array, T element) {
			if (array is null || array.IsNullOrEmpty) {
				return SingleElement<T>(element);
			}

			return new ElementPrependedArray<T>(array, element);
		}

		/// <summary>
		/// Inserts <paramref name="element"/> into the <paramref name="array"/> on specified <paramref name="index"/> in new immutable array.
		/// </summary>
		/// <typeparam name="T">Type of elements in array.</typeparam>
		/// <param name="array">Base array.</param>
		/// <param name="index">Index where <paramref name="element"/> will be inserted.</param>
		/// <param name="element">Element to insert.</param>
		/// <returns></returns>
		public static ImArray<T> Insert<T>(ImArray<T> array, int index, T element) {
			if (array is null || array.IsNullOrEmpty) {
				return SingleElement<T>(element);
			}

			return new ElementInsertedArray<T>(array, index, element);
		}

		/// <summary>
		/// Creates immutble range from passed <paramref name="array"/> starting at passed <paramref name="startIndex"/> to the end of array.
		/// </summary>
		/// <remarks>
		/// This method is tolerant to invalid arguments and will try to return correct results.
		/// It means both <paramref name="array"/> and <paramref name="startIndex"/> parameters are fully checked and the values are modified for correct results.
		/// </remarks>
		/// <example><![CDATA[
		/// var nullArray = ImArray.Null<int>();
		/// ImArray.Subset(nullArray, 0); // returns NullArray
		/// ImArray.Subset(nullArray, 1) // returns NullArray
		/// 
		/// var emptyArray ImArray.Empty<int>();
		/// ImArray.Subset(emptyArray, 10); // returns empty array
		/// 
		/// var array1 = ImArray.Create<int>(new int[]{ 5 });
		/// ImArray.Subset(array1, 10); // returns empty array
		/// ImArray.Subset(array1, 0);   // returns same array
		/// 
		/// var array2 = ImArray.Create<int>(new int[] { 5, 7, 9 , 11, 13 });
		/// ImArray.Subset(array2, 10); // returns empty array
		/// ImArray.Subset(array, 0);    // returns same instance (array2), because the subset is exactly same as input
		/// ImArray.Subset(array2, 1);  // returns SubsetArray with "7, 9, 11, 13";
		/// ]]></example>
		/// <typeparam name="T">Type of elements in array.</typeparam>
		/// <param name="array">Array from the subset should be created. Can be null. Can be empty.</param>
		/// <param name="startIndex">Start index of new subset. Can be anything.</param>
		/// <returns></returns>
		public static ImArray<T> Range<T>(ImArray<T> array, int startIndex) => Range<T>(array, startIndex, int.MaxValue);

		/// <summary>
		/// Returns range from passed <paramref name="array"/> starting at passed <paramref name="startIndex"/> with passed <paramref name="length"/>.
		/// </summary>
		/// <remarks>
		/// This method is tolerant to invalid arguments and will try to return correct results.
		/// It means both <paramref name="array"/> and <paramref name="startIndex"/> and <paramref name="length"/> parameters are fully checked and the values are modified for correct results.
		/// </remarks>
		/// <example><![CDATA[
		/// var nullArray = ImArray.Null<int>();
		/// ImArray.Subset(nullArray, 0, 0); // returns NullArray
		/// ImArray.Subset(nullArray, 1, 10) // returns NullArray
		/// 
		/// var emptyArray ImArray.Empty<int>();
		/// ImArray.Subset(emptyArray, 10, 50); // returns empty array
		/// 
		/// var array1 = ImArray.Create<int>(new int[]{ 5 });
		/// ImArray.Subset(array1, 10, 50); // returns empty array
		/// ImArray.Subset(array1, 0, 1);   // returns same array
		/// 
		/// var array2 = ImArray.Create<int>(new int[] { 5, 7, 9 , 11, 13 });
		/// ImArray.Subset(array2, 10, 50); // returns empty array
		/// ImArray.Subset(array, 0, 5);    // returns same instance (array2), because the subset is exactly same as input
		/// ImArray.Subset(array2, 0, 1);   // returns SubsetArray with "5"
		/// ImArray.SubSet(array2, 1, 3);   // returns SubsetArray with "7, 9, 11"
		/// ImArray.Subset(array2, 1, 10);  // retzrbs SubsetArray with "7, 9, 11, 13";
		/// ImArray.Subset(array2, 1, 25);  // retzrbs SubsetArray with "7, 9, 11, 13";
		/// ]]></example>
		/// <typeparam name="T">Type of elements in array.</typeparam>
		/// <param name="array">Array from the subset should be created. Can not be null.</param>
		/// <param name="startIndex">Start index of new subset.</param>
		/// <param name="length">The length of subset.</param>
		/// <returns></returns>
		public static ImArray<T> Range<T>(ImArray<T> array, int startIndex, int length) {
			if (array.IsNullOrEmpty) {
				return array;
			}
			if (length < 1) {
				return Empty<T>();
			}

			var finalStartIndex = startIndex < 0 ? startIndex : startIndex;

			var arrLength = array.Length;
			if (0 == finalStartIndex && arrLength == length) {
				return array;
			}

			var maxLength = arrLength - finalStartIndex;
			if (maxLength < 1) {
				return Empty<T>();
			}

			var finalLength = Math.Min(length, maxLength);
			return new RangeArray<T>(false, array, finalStartIndex, finalLength);
		}

		/// <summary>
		/// Returns range from passed <paramref name="array"/> starting at passed <paramref name="startIndex"/> to the end of array.
		/// </summary>
		/// <remarks>
		/// This method IS NOT TOLERANT to invalid arguments and will throw exceptions if any argument is invalid.
		/// </remarks>
		/// <typeparam name="T">Type of elements in array.</typeparam>
		/// <param name="array">Array from the subset should be created. Can NOT be null. Can be empty.</param>
		/// <param name="startIndex">Start index of new subset. Must be in range of input <paramref name="array"/>.</param>
		/// <returns></returns>
		public static ImArray<T> RangeExact<T>(ImArray<T> array, int startIndex) {
			if (array is null) { throw new ArgumentNullException(nameof(array)); }

			var length = array.Length - startIndex;
			return RangeExact<T>(array, startIndex, length);
		}

		/// <summary>
		/// Returns range from passed <paramref name="array"/> starting at passed <paramref name="startIndex"/> with passed <paramref name="length"/>.
		/// </summary>
		/// <typeparam name="T">Type of elements in array.</typeparam>
		/// <param name="array">Array from the subset should be created. Can NOT be null. Can be empty.</param>
		/// <param name="startIndex">Start index of new subset. Must be in range of input <paramref name="array"/>.</param>
		/// <param name="length">The length of subset.</param>
		/// <returns></returns>
		public static ImArray<T> RangeExact<T>(ImArray<T> array, int startIndex, int length) => new RangeArray<T>(array, startIndex, length);

		/// <summary>
		/// Shortcut for <code>Range(array, O, length)</code>.
		/// </summary>
		public static ImArray<T> RangeLeft<T>(ImArray<T> array, int length) => Range<T>(array, 0, length);

		/// <summary>
		/// Shortcut for <code>RangeExact(array, O, length)</code>.
		/// </summary>
		public static ImArray<T> RangeLeftExact<T>(ImArray<T> array, int length) => RangeExact<T>(array, 0, length);

		/// <summary>
		/// Shortcut for <code>Range(array, array.Length - length, length)</code>.
		/// </summary>
		public static ImArray<T> RangeRight<T>(ImArray<T> array, int length) => Range<T>(array, array.Length - length, length);

		/// <summary>
		/// Shortcut for <code>RangeExact(array, array.Length - length, length)</code>.
		/// </summary>
		public static ImArray<T> RangeRightExact<T>(ImArray<T> array, int length) {
			var startIndex = array.Length - length;
			return RangeExact<T>(array, startIndex, length);
		}

		/// <summary>
		/// Creates reveresed immutable array based on <paramref name="array"/>.
		/// </summary>
		/// <typeparam name="T">Type of elements in array.</typeparam>
		/// <param name="array">Array used as source for result.</param>
		/// <returns></returns>
		public static ImArray<T> Reverse<T>(ImArray<T> array) => new ReversedArray<T>(array);

		/// <summary>
		/// Creates repeated immutable array based on <paramref name="array"/> and <paramref name="repetitions"/>.
		/// </summary>
		/// <typeparam name="T">Type of elements in array.</typeparam>
		/// <param name="array">Array used as source for result.</param>
		/// <param name="repetitions">Number of repetitions of <paramref name="array"/> in result.</param>
		/// <returns></returns>
		public static ImArray<T> Repeat<T>(ImArray<T> array, int repetitions) => new RepeatedArray<T>(array, repetitions);

		/// <summary>
		/// Returns immutable array which is based on pure array (<see cref="Array"/>).
		/// If the input array (<paramref name="array"/>) is composed array (i.e. <see cref="ConcatedArray{T}"/>, <see cref="RangeArray{T}"/>, etc...),
		/// the new instance of immutable array will be returned.
		/// 
		/// If the input array is not composed from another arrays, the original array (<paramref name="array"/>) will be returned.
		/// </summary>
		/// <typeparam name="T">Type of elements in array.</typeparam>
		/// <param name="array">Array which shall be flatten.</param>
		/// <returns></returns>
		public static ImArray<T> Flatten<T>(ImArray<T> array) {
			if (array is null || !array.IsComposed) { return array; }

			var pureArray = array.ToArray();
			return Create<T>(pureArray);
		}
		#endregion Factory methods
	}
}
