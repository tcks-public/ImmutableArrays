﻿/* Licenced under MIT. Complete license is in LINCENSE.txt in root. */

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace TcKs.ImmutableArrays {
	/// <summary>
	/// Generic variant of base class<see cref="ImArray"/>.
	/// </summary>
	/// <typeparam name="T">Type of elements in array.</typeparam>
	public abstract partial class ImArray<T> : ImArray, IEnumerable<T> {
		/// <inheritdoc />
		public override Type ElementType => typeof(T);

		#region Index members
		/// <summary>
		/// Generic variant of <see cref="this[int]"/>.
		/// </summary>
		/// <param name="index">Index of element.</param>
		/// <returns></returns>
		public abstract new T this[int index] { get; }

		/// <inheritdoc />
		protected override object GetElementAt(int index) => this[index];
		#endregion Index members

		#region Enumeration members
		/// <inheritdoc />
		public virtual IEnumerator<T> GetEnumerator() => new ImmutableArrayEnumerator<T>(this);

		/// <inheritdoc />
		protected override IEnumerator IEnumerator_GetEnumerator() => this.GetEnumerator();
		#endregion Enumeration members

		#region Constructors
		/// <summary>
		/// Internal constructor.
		/// </summary>
		internal ImArray() { }
		#endregion Constructors
	}

	partial class ImArray<T> : IReadOnlyList<T> {
		/// <summary>
		/// The count of elements.
		/// Same as <see cref="ImArray.Length"/>¨.
		/// </summary>
		public int Count => this.Length;
	}

	partial class ImArray<T> : IEquatable<ImArray<T>> {
		/// <summary>
		/// Implementation of <see cref="IEquatable{T}.Equals(T)"/>.
		/// </summary>
		/// <param name="other"></param>
		/// <returns></returns>
		public virtual bool Equals(ImArray<T> other) => this.Equals(other, EqualityComparer<T>.Default);

		/// <inheritdoc />
		public override int GetHashCode() => this.GetHashCode(EqualityComparer<T>.Default);
	}

	partial class ImArray<T> : IStructuralEquatable {
		/// <summary>
		/// Implementation of <see cref="IStructuralEquatable.Equals(object, IEqualityComparer)"/>.
		/// </summary>
		public virtual bool Equals(object other, IEqualityComparer comparer) {
			if (comparer is null) { throw new ArgumentNullException(nameof(comparer)); }

			if (other is null) { return this.IsNull; }

			var otherArray = other as ImArray;
			if (otherArray is null) { return false; }

			if (this.IsEmpty != otherArray.IsEmpty) { return false; }
			if (this.IsNull != otherArray.IsNull) { return false; }

			var length = this.Length;
			if (otherArray.Length != length) { return false; }

			for (var i = 0; i < length; i++) {
				var item = this[i];
				var otherItem = otherArray[i];

				if (!comparer.Equals(item, otherItem)) {
					return false;
				}
			}


			return true;
		}

		/// <summary>
		/// Implementation of <see cref="IStructuralEquatable.GetHashCode(IEqualityComparer)"/>.
		/// </summary>
		/// <param name="comparer"></param>
		/// <returns></returns>
		public int GetHashCode(IEqualityComparer comparer) {
			if (comparer is null) { throw new ArgumentNullException(nameof(comparer)); }

			int result = 0;
			foreach (var item in this) {
				var itemCode = comparer.GetHashCode(item);
				result = (result * 31) + itemCode;
			}

			return result;
		}
	}

	partial class ImArray<T> : IStructuralComparable {
		/// <summary>
		/// Implementation of <see cref="IStructuralComparable.CompareTo(object, IComparer)"/>.
		/// </summary>
		public int CompareTo(object other, IComparer comparer) {
			if (comparer is null) { throw new ArgumentNullException(nameof(comparer)); }

			if (other is null) {
				if (this.IsNull) { return 0; }
				else { return 1; }
			}

			var otherArray = other as ImArray;
			if (otherArray is null) {
				throw new ArgumentException("Other object is not immutable array.", nameof(other));
			}

			var selfLength = this.Length;
			var otherLength = otherArray.Length;
			if (selfLength != otherLength) {
				throw new ArgumentException("Other array has different length.", nameof(other));
			}

			for (var i = 0; i < selfLength; i++) {
				var selfItem = this[i];
				var otherItem = otherArray[i];
				var result = comparer.Compare(selfItem, otherItem);
				if (0 != result) { return result; }
			}

			return 0;
		}
	}

	partial class ImArray<T> : IList<T> {
		private Exception CanNotModifyImmutableArray() => new InvalidOperationException("Can not modify immutable array.");

		/// <inheritdoc />
		public int IndexOf(T item) {
			var length = this.Length;
			for (var i = 0; i < length; i++) {
				var arrItem = this[i];
				if (object.Equals(arrItem, item)) {
					return i;
				}
			}

			return -1;
		}

		void IList<T>.Insert(int index, T item) => throw CanNotModifyImmutableArray();

		void IList<T>.RemoveAt(int index) => throw CanNotModifyImmutableArray();

		T IList<T>.this[int index] {
			get => this[index];
			set => throw CanNotModifyImmutableArray();
		}

		void ICollection<T>.Add(T item) => throw CanNotModifyImmutableArray();

		void ICollection<T>.Clear() => throw CanNotModifyImmutableArray();

		/// <inheritdoc />
		public bool Contains(T item) => this.IndexOf(item) >= 0;

		/// <inheritdoc />
		public void CopyTo(T[] array, int arrayIndex) {
			var counter = arrayIndex;
			foreach (var item in this) {
				array[counter] = item;
				counter++;
			}
		}

		bool ICollection<T>.Remove(T item) => throw CanNotModifyImmutableArray();

		bool ICollection<T>.IsReadOnly => true;
	}
}
