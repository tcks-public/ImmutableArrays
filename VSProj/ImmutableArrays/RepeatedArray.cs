﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TcKs.ImmutableArrays {
	/// <summary>
	/// One array created from one repeated array.
	/// </summary>
	/// <typeparam name="T">Type of elements in array.</typeparam>
	public sealed class RepeatedArray<T> : ImArray<T> {
		#region Source
		private readonly ImArray<T> source;

		/// <summary>
		/// The source array from which this array is constructed.
		/// </summary>
		public ImArray<T> Source => this.source;
		#endregion Source

		#region Repetitions
		private readonly int repetitions;

		/// <summary>
		/// Number of repetion of source array.
		/// </summary>
		public int Repetitions => this.repetitions;
		#endregion Repetitions

		#region Index members
		/// <inheritdoc />
		public override T this[int index] => this.source[index % this.source.Length];
		#endregion Index members

		#region Nullity, emptiness & length members
		/// <inheritdoc />
		public override int Length => this.source.Length * this.repetitions;

		/// <inheritdoc />
		public override bool IsNull => this.source.IsNull;

		/// <inheritdoc />
		public override bool IsEmpty => this.source.IsEmpty;

		/// <inheritdoc />
		public override bool IsNullOrEmpty => this.source.IsNullOrEmpty;
		#endregion Nullity, emptiness & length members

		#region Composition members
		/// <inheritdoc />
		public override bool IsComposed => true;
		#endregion Composition members

		#region Constructors
		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="source">Source array. Can not be null.</param>
		/// <param name="repetitions">Number of repetions of <paramref name="source"/> array. Must be greater than 0.</param>
		public RepeatedArray(ImArray<T> source, int repetitions) {
			this.source = source ?? throw new ArgumentNullException(nameof(source));
			this.repetitions = repetitions > 0 ? repetitions : throw new ArgumentOutOfRangeException(nameof(repetitions));
		}
		#endregion Constructors
	}
}
