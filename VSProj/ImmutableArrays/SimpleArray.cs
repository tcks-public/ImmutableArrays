﻿/* Licenced under MIT. Complete license is in LINCENSE.txt in root. */

using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace TcKs.ImmutableArrays
{
	/// <summary>
	/// Immutable array composed from elements of array.
	/// </summary>
	/// <typeparam name="T">Type of elements in array.</typeparam>
	public sealed class SimpleArray<T> : ImArray<T> {
		#region Source
		private readonly T[] items;
		#endregion Source

		#region Index members
		/// <inheritdoc />
		public override T this[int index] => this.items[index];
		#endregion Index members

		#region Nullity, emptiness & length members
		/// <inheritdoc />
		public override int Length => this.items.Length;

		/// <inheritdoc />
		public override bool IsNull => false;
		#endregion Nullity, emptiness & length members

		#region Composition members
		/// <inheritdoc />
		public override bool IsComposed => false;
		#endregion Composition members

		#region Constructors
		/// <summary>
		/// Internal constructor for internal use only!
		/// </summary>
		/// <param name="checkInputs">
		/// The <c>true</c> means the input parameters will be checked for validity.
		/// Othervise none of input parameters will be checked for validity.
		/// </param>
		/// <param name="items">Elements of array.</param>
		internal SimpleArray(bool checkInputs, T[] items) {
			if (checkInputs) {
				if (items is null) { throw new ArgumentNullException(nameof(items)); }
			}

			this.items = items;
		}

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="items">Elements of new array. Can not be null. Can be empty.</param>
		public SimpleArray(IEnumerable<T> items) {
			if (items is null) { throw new ArgumentNullException(nameof(items)); }

			// even if 'items' is array, we want create new array,
			// because we need guaranty immuatability
			this.items = items.ToArray();
		}
		#endregion Constructors
	}
}
