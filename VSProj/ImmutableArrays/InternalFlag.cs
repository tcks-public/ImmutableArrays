﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TcKs.ImmutableArrays {
	/// <summary>
	/// Contains flags intended only for internal purposes.
	/// </summary>
	internal enum InternalFlag {
		None
	}
}
