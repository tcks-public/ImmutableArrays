﻿/* Licenced under MIT. Complete license is in LINCENSE.txt in root. */

using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace TcKs.ImmutableArrays
{
	/// <summary>
	/// Contains extension methods related to immutable arrays.
	/// </summary>
	public static class ImArrayExtensions {
		#region Conversions
		/// <summary>
		/// Shortcut for <code>ImArray.Create(<paramref name="self"/>)</code>.
		/// </summary>
		public static ImArray<T> ToImArray<T>(this IEnumerable<T> self) => ImArray.Create<T>(self);
		#endregion Conversions

		#region Concat
		/// <summary>
		/// Shortcut for <code>ImArray.Concat(<paramref name="self"/>, <paramref name="arrays"/>)</code>.
		/// </summary>
		public static ImArray<T> Concat<T>(this ImArray<T> self, IEnumerable<ImArray<T>> arrays) => ImArray.Concat<T>(self, arrays);

		/// <summary>
		/// Shortcut for <code>ImArray.Concat(<paramref name="self"/>, <paramref name="array"/>)</code>.
		/// </summary>
		/// <remarks>
		/// Can not be named 'Concat' because there is conflict in names with <see cref="ImArray.Concat{T}(ImArray{T}[])"/>.
		/// </remarks>
		public static ImArray<T> ConcatOne<T>(this ImArray<T> self, ImArray<T> array) => ImArray.Concat<T>(self, array);

		/// <summary>
		/// Shortcut for <code>ImArray.Concat(<paramref name="self"/>, <paramref name="arrays"/>)</code>.
		/// </summary>
		public static ImArray<T> Concat<T>(this ImArray<T> self, params ImArray<T>[] arrays) => ImArray.Concat<T>(self, arrays);
		#endregion Concat

		#region Insert
		/// <summary>
		/// Shortcut for <code>ImArray.InsertOne(<paramref name="self"/>, <paramref name="index"/>, <paramref name="array"/>)</code>.
		/// </summary>
		public static ImArray<T> InsertOne<T>(this ImArray<T> self, int index, ImArray<T> array) => ImArray.InsertOne(self, index, array);

		/// <summary>
		/// Shortcut for <code>ImArray.Insert(<paramref name="self"/>, <paramref name="index"/>, <paramref name="arrays"/>)</code>.
		/// </summary>
		public static ImArray<T> Insert<T>(this ImArray<T> self, int index, IEnumerable<ImArray<T>> arrays) => ImArray.Insert(self, index, arrays);

		/// <summary>
		/// Shortcut for <code>ImArray.Insert(<paramref name="self"/>, <paramref name="index"/>, <paramref name="arrays"/>)</code>.
		/// </summary>
		public static ImArray<T> Insert<T>(this ImArray<T> self, int index, params ImArray<T>[] arrays) => ImArray.Insert(self, index, arrays);
		#endregion Insert

		#region Append, Prepend, Insert
		/// <summary>
		/// Shortcut for <code>ImArray.Append(<paramref name="self"/>, <paramref name="element"/>)</code>.
		/// </summary>
		public static ImArray<T> Append<T>(this ImArray<T> self, T element) => ImArray.Append<T>(self, element);

		/// <summary>
		/// Shortcut for <code>ImArray.Prepend(<paramref name="self"/>, <paramref name="element"/>)</code>.
		/// </summary>
		public static ImArray<T> Prepend<T>(this ImArray<T> self, T element) => ImArray.Prepend<T>(self, element);

		/// <summary>
		/// Shortcut for <code>ImArray.Insert(<paramref name="self"/>, <paramref name="index"/>, <paramref name="element"/>)</code>.
		/// </summary>
		public static ImArray<T> Insert<T>(this ImArray<T> self, int index, T element) => ImArray.Insert<T>(self, index, element);
		#endregion Append, Prepend, Insert

		#region Range
		/// <summary>
		/// Shortcut for <code>ImArray.Range(<paramref name="self"/>, <paramref name="startIndex"/>)</code>.
		/// </summary>
		public static ImArray<T> Range<T>(this ImArray<T> self, int startIndex) => ImArray.Range<T>(self, startIndex);

		/// <summary>
		/// Shortcut for <code>ImArray.Range(<paramref name="self"/>, <paramref name="startIndex"/>, <paramref name="length"/>)</code>.
		/// </summary>
		public static ImArray<T> Range<T>(this ImArray<T> self, int startIndex, int length) => ImArray.Range<T>(self, startIndex, length);

		/// <summary>
		/// Shortcut for <code>ImArray.RangeExact(<paramref name="self"/>, <paramref name="startIndex"/>)</code>.
		/// </summary>
		public static ImArray<T> RangeExact<T>(this ImArray<T> self, int startIndex) => ImArray.RangeExact<T>(self, startIndex);

		/// <summary>
		/// Shortcut for <code>ImArray.RangeExact(<paramref name="self"/>, <paramref name="startIndex"/>, <paramref name="length"/>)</code>.
		/// </summary>
		public static ImArray<T> RangeExact<T>(this ImArray<T> self, int startIndex, int length) => ImArray.RangeExact<T>(self, startIndex, length);

		/// <summary>
		/// Shortcut for <code>ImArray.RangeLeft(<paramref name="self"/>, <paramref name="length"/>)</code>.
		/// </summary>
		public static ImArray<T> RangeLeft<T>(this ImArray<T> self, int length) => ImArray.RangeLeft<T>(self, length);

		/// <summary>
		/// Shortcut for <code>ImArray.RangeLeftExact(<paramref name="self"/>, <paramref name="length"/>)</code>.
		/// </summary>
		public static ImArray<T> RangeLeftExact<T>(this ImArray<T> self, int length) => ImArray.RangeLeftExact<T>(self, length);

		/// <summary>
		/// Shortcut for <code>ImArray.RangeRight(<paramref name="self"/>, <paramref name="length"/>)</code>.
		/// </summary>
		public static ImArray<T> RangeRight<T>(this ImArray<T> self, int length) => ImArray.RangeRight<T>(self, length);

		/// <summary>
		/// Shortcut for <code>ImArray.RangeRightExact(<paramref name="self"/>, <paramref name="length"/>)</code>.
		/// </summary>
		public static ImArray<T> RangeRightExact<T>(this ImArray<T> self, int length) => ImArray.RangeRightExact<T>(self, length);
		#endregion Range

		#region Reverse
		/// <summary>
		/// Shortcut for <code>ImArray.Reverse(<paramref name="self"/>)</code>.
		/// </summary>
		public static ImArray<T> Reverse<T>(this ImArray<T> self) => ImArray.Reverse<T>(self);
		#endregion Reverse

		#region Repeat
		/// <summary>
		/// Shortcut for <code>ImArray.Repeat(<paramref name="self"/>, <paramref name="repetitions"/>)</code>.
		/// </summary>
		public static ImArray<T> Repeat<T>(this ImArray<T> self, int repetitions) => ImArray.Repeat<T>(self, repetitions);
		#endregion Repeat
	}
}
