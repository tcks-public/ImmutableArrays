﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TcKs.ImmutableArrays {
	/// <summary>
	/// Composed immutable array where one element is appended to some other immutable array.
	/// </summary>
	/// <typeparam name="T">Type of elements in array.</typeparam>
	public sealed class ElementAppendedArray<T> : ImArray<T> {
		#region Source
		private readonly ImArray<T> source;

		/// <summary>
		/// The source array from which this array is constructed.
		/// </summary>
		public ImArray<T> Source => this.source;

		private readonly T element;

		/// <summary>
		/// The appended element.
		/// </summary>
		public T Element => this.element;
		#endregion Source

		#region Index members
		/// <inheritdoc />
		public override T this[int index] {
			get {
				var currLength = this.Length;
				if (index == currLength - 1) {
					return this.element;
				}

				return this.source[index];
			}
		}
		#endregion Index members

		#region Nullity, emptiness & length members
		/// <inheritdoc />
		public override int Length => this.source.Length + 1;

		/// <inheritdoc />
		public override bool IsNull => false;

		/// <inheritdoc />
		public override bool IsEmpty => false;

		/// <inheritdoc />
		public override bool IsNullOrEmpty => false;
		#endregion Nullity, emptiness & length members

		#region Composition members
		/// <inheritdoc />
		public override bool IsComposed => true;
		#endregion Composition members

		#region Constructors
		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="source">Source array. Can not be null.</param>
		/// <param name="element">Appended element.</param>
		public ElementAppendedArray(ImArray<T> source, T element) {
			this.source = source ?? throw new ArgumentNullException(nameof(source));
			this.element = element;
		}
		#endregion Constructors
	}
}
