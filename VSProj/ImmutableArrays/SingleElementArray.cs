﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TcKs.ImmutableArrays {
	/// <summary>
	/// The flat immutable array with single element.
	/// </summary>
	/// <typeparam name="T">Type of element in array.</typeparam>
	public sealed class SingleElementArray<T> : ImArray<T> {
		#region Source
		private readonly T element;

		/// <summary>
		/// Contained element.
		/// </summary>
		public T Element => this.element;
		#endregion Source

		#region Index members
		/// <inheritdoc />
		public override T this[int index] => 0 == index ? this.element : throw new IndexOutOfRangeException();
		#endregion Index members

		#region Nullity, emptiness & length members
		/// <inheritdoc />
		public override int Length => 1;

		/// <inheritdoc />
		public override bool IsEmpty => false;

		/// <inheritdoc />
		public override bool IsNull => false;

		/// <inheritdoc />
		public override bool IsNullOrEmpty => false;
		#endregion Nullity, emptiness & length members

		#region Composition members
		/// <inheritdoc />
		public override bool IsComposed => false;
		#endregion Composition members

		#region Constructors
		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="element">Item which will be as the single element in this array.</param>
		public SingleElementArray(T element) {
			this.element = element;
		}
		#endregion Constructors
	}
}
