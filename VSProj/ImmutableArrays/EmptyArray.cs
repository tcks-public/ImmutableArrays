﻿/* Licenced under MIT. Complete license is in LINCENSE.txt in root. */

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace TcKs.ImmutableArrays {
	/// <summary>
	/// Empty immutable array.
	/// </summary>
	/// <typeparam name="T">Type of elements in array.</typeparam>
	public sealed partial class EmptyArray<T> : ImArray<T> {
		#region Nullity, emptiness & length members
		/// <inheritdoc />
		public override int Length => 0;

		/// <inheritdoc />
		public override bool IsEmpty => true;

		/// <inheritdoc />
		public override bool IsNull => false;

		/// <inheritdoc />
		public override bool IsNullOrEmpty => true;
		#endregion Nullity, emptiness & length members

		#region Index members
		/// <summary>
		/// Always throws <see cref="IndexOutOfRangeException"/> because this array has 0 elements.
		/// </summary>
		/// <param name="index">Index of element to return.</param>
		/// <returns></returns>
		public override T this[int index] => throw new IndexOutOfRangeException();
		#endregion Index members

		#region Composition members
		/// <inheritdoc />
		public override bool IsComposed => false;
		#endregion Composition members

		/// <summary>
		/// Constructor.
		/// </summary>
		public EmptyArray() { }
	}

	partial class EmptyArray<T> {
		/// <summary>
		/// Returns <c>true</c> if <paramref name="obj"/> is <see cref="ImArray{T}"/> and <see cref="IsEmpty"/> and is not <see cref="IsNull"/>.
		/// Othervise returns <c>false</c>.
		/// </summary>
		/// <param name="obj">Object to compare.</param>
		/// <returns></returns>
		public override bool Equals(object obj) => obj is ImArray<T> arr && arr.IsEmpty && !arr.IsNull;

		/// <summary>
		/// Returns <c>true</c> if <paramref name="other"/> is <see cref="IsEmpty"/> and is not <see cref="IsNull"/>.
		/// Othervise returns <c>false</c>.
		/// </summary>
		/// <param name="other">Array to compare.</param>
		/// <returns></returns>
		public override bool Equals(ImArray<T> other) => !(other is null) && !other.IsNull && other.IsEmpty;

		/// <summary>
		/// Returns 0;
		/// </summary>
		/// <returns></returns>
		public override int GetHashCode() => 0;
	}

	partial class EmptyArray<T> : IEnumerator<T> {
		/// <summary></summary>
		/// <remarks>
		/// This class implements both <see cref="IEnumerable{T}"/> and <see cref="IEnumerator{T}"/>.
		/// Because empty array has no element -> enumerator don't need any state -> can be hardcoded -> no other instance of object is needed.
		/// </remarks>
		/// <returns></returns>
		public override IEnumerator<T> GetEnumerator() => this;

		/// <summary>
		/// Always throws <see cref="IndexOutOfRangeException"/>.
		/// </summary>
		[System.Diagnostics.DebuggerHidden]
		public T Current => throw new IndexOutOfRangeException();

		[System.Diagnostics.DebuggerHidden]
		object IEnumerator.Current => this.Current;

		/// <summary>
		/// Always returns <c>false</c>.
		/// </summary>
		/// <returns></returns>
		public bool MoveNext() => false;

		/// <summary>
		/// Do nothing.
		/// </summary>
		public void Reset() { }

		/// <summary>
		/// Do nothing.
		/// </summary>
		public void Dispose() { }
	}
}
