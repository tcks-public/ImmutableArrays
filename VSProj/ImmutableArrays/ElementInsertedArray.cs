﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TcKs.ImmutableArrays {
	/// <summary>
	/// Composed immutable array where one elmenet is inserted into another immutable array.
	/// </summary>
	/// <typeparam name="T">Type of elements in array.</typeparam>
	public sealed class ElementInsertedArray<T> : ImArray<T> {
		#region Source
		private readonly ImArray<T> source;

		/// <summary>
		/// The source array from which this array is constructed.
		/// </summary>
		public ImArray<T> Source => this.source;

		private readonly int insertIndex;

		/// <summary>
		/// Index where is <see cref="Element"/> inserted.
		/// </summary>
		public int InsertIndex => this.insertIndex;

		private readonly T element;

		/// <summary>
		/// The inserted element.
		/// </summary>
		public T Element => this.element;
		#endregion Source

		#region Index members
		/// <inheritdoc />
		public override T this[int index] {
			get {
				var elIndex = this.insertIndex;
				if (index < elIndex) {
					return this.source[index];
				}
				else if (index > elIndex) {
					return this.source[index - 1];
				}

				return this.element;
			}
		}
		#endregion Index members

		#region Nullity, emptiness & length members
		/// <inheritdoc />
		public override int Length => this.source.Length + 1;

		/// <inheritdoc />
		public override bool IsNull => false;

		/// <inheritdoc />
		public override bool IsEmpty => false;

		/// <inheritdoc />
		public override bool IsNullOrEmpty => false;
		#endregion Nullity, emptiness & length members

		#region Composition members
		/// <inheritdoc />
		public override bool IsComposed => true;
		#endregion Composition members

		#region Constructors
		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="source">Source array. Can not be null.</param>
		/// <param name="insertIndex">The index where will be <paramref name="element"/> inserted.</param>
		/// <param name="element">Inserted element.</param>
		public ElementInsertedArray(ImArray<T> source, int insertIndex, T element) {
			this.source = source ?? throw new ArgumentNullException(nameof(source));
			this.element = element;

			if (insertIndex < 0 || insertIndex > source.Length) { throw new ArgumentOutOfRangeException(nameof(insertIndex)); }
			this.insertIndex = insertIndex;
		}
		#endregion Constructors
	}
}
