﻿/* Licenced under MIT. Complete license is in LINCENSE.txt in root. */

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace TcKs.ImmutableArrays
{
	/// <summary>
	/// Implementation of <see cref="IEnumerator{T}"/> for <see cref="ImArray{T}"/>.
	/// </summary>
	/// <typeparam name="T">Type of elements in array.</typeparam>
	public sealed class ImmutableArrayEnumerator<T> : IEnumerator<T> {
		/// <summary>
		/// Enumerated array.
		/// </summary>
		private readonly ImArray<T> array;

		/// <summary>
		/// Current index of enumeration.
		/// </summary>
		private int index;

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="arrray">Array used to iteration. Can not be null.</param>
		public ImmutableArrayEnumerator(ImArray<T> arrray) {
			if (arrray is null) { throw new ArgumentNullException(nameof(arrray)); }

			this.index = -1;
			this.array = arrray;
		}

		/// <inheritdoc />
		public T Current => this.array[index];
		object IEnumerator.Current => this.Current;

		/// <inheritdoc />
		public bool MoveNext() {
			var newIndex = this.index + 1;
			if (newIndex < this.array.Length) {
				this.index = newIndex;
				return true;
			}

			return false;
		}

		/// <inheritdoc />
		public void Reset() {
			this.index = -1;
		}

		/// <inheritdoc />
		public void Dispose() {
			// just because we want to be sure
			this.index = int.MinValue;
		}
	}
}
