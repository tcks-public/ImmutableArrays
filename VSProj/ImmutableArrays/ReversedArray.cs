﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TcKs.ImmutableArrays {
	/// <summary>
	/// One array with reversed order of elements.
	/// </summary>
	/// <typeparam name="T">Type of elements in array.</typeparam>
	public sealed class ReversedArray<T> : ImArray<T> {
		#region Source
		private readonly ImArray<T> source;

		/// <summary>
		/// The source array from which this array is constructed.
		/// </summary>
		public ImArray<T> Source => this.source;
		#endregion Source

		#region Index members
		/// <inheritdoc />
		public override T this[int index] => this.source[this.source.Length - 1 - index];
		#endregion Index members

		#region Nullity, emptiness & length members
		/// <inheritdoc />
		public override int Length => this.source.Length;

		/// <inheritdoc />
		public override bool IsNull => this.source.IsNull;

		/// <inheritdoc />
		public override bool IsEmpty => this.source.IsEmpty;

		/// <inheritdoc />
		public override bool IsNullOrEmpty => this.source.IsNullOrEmpty;
		#endregion Nullity, emptiness & length members

		#region Composition members
		/// <inheritdoc />
		public override bool IsComposed => true;
		#endregion Composition members

		#region Constructors
		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="source">Source array. Can not be null.</param>
		public ReversedArray(ImArray<T> source) {
			this.source = source ?? throw new ArgumentNullException(nameof(source));
		}
		#endregion Constructors
	}
}
