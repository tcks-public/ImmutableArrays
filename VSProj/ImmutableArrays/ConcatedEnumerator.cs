﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace TcKs.ImmutableArrays {
	partial class ConcatedArray<T> {
		/// <summary>
		/// Special implementation of <see cref="IEnumerator{T}"/> for <see cref="ConcatedArray{T}"/>.
		/// </summary>
		public sealed class ConcatedArrayEnumerator : IEnumerator<T> {
			private readonly ConcatedEnumerator<T> innerEnumerator;

			/// <summary>
			/// Constructor.
			/// </summary>
			/// <param name="source">Array used for enumeration.</param>
			public ConcatedArrayEnumerator(ConcatedArray<T> source) {
				if (source is null) { throw new ArgumentNullException(nameof(source)); }

				var enumerators = this.GetEnumerators(source).GetEnumerator();
				this.innerEnumerator = new ConcatedEnumerator<T>(enumerators);
			}

			private IEnumerable<IEnumerable<T>> GetEnumerators(ImArray<T> array) {
				if (array is ConcatedArray<T> concatedArray) {
					foreach (var innerArray in concatedArray.source) {
						foreach (var innerEnumerator in this.GetEnumerators(innerArray)) {
							yield return innerEnumerator;
						}
					}
				}
				else {
					yield return array;
				}
			}


			/// <inheritdoc />
			public T Current => this.innerEnumerator.Current;
			object IEnumerator.Current => this.Current;

			/// <inheritdoc />
			public bool MoveNext() => this.innerEnumerator.MoveNext();

			/// <inheritdoc />
			public void Reset() => this.innerEnumerator.Reset();

			/// <inheritdoc />
			public void Dispose() => this.innerEnumerator.Dispose();
		}
	}
}
