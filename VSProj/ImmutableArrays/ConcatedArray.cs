﻿/* Licenced under MIT. Complete license is in LINCENSE.txt in root. */

using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace TcKs.ImmutableArrays
{
	/// <summary>
	/// One array created by concating other arrays.
	/// </summary>
	/// <typeparam name="T">Type of elements in array.</typeparam>
	public sealed partial class ConcatedArray<T> : ImArray<T> {
		#region Source
		private readonly ImArray<ImArray<T>> source;

		/// <summary>
		/// The arrays from which are this array concated.
		/// </summary>
		public ImArray<ImArray<T>> Source => this.source;
		#endregion Source

		#region Nullity, emptiness & length members
		private readonly int length;
		/// <inheritdoc />
		public override int Length => this.length;

		private readonly bool isNull;
		/// <inheritdoc />
		public override bool IsNull => this.isNull;

		/// <inheritdoc />
		public override bool IsNullOrEmpty => false;
		#endregion Nullity, emptiness & length members

		#region Index members
		/// <inheritdoc />
		public override T this[int index] {
			get {
				if (index < 0 || index >= length) { throw new IndexOutOfRangeException(); }

				return InternalHelper.GetElementAt(this.source, index);
			}
		}
		#endregion Index members

		#region Composition members
		/// <inheritdoc />
		public override bool IsComposed => true;
		#endregion Composition members

		#region Constructors
		/// <summary>
		/// Internal constructor.
		/// </summary>
		/// <param name="internalFlag">Value is ignored. This parameter is used to distinction between internal and public constructor.</param>
		/// <param name="sourceArrays">Arrays which will be used for final immutable array.</param>
		internal ConcatedArray(InternalFlag internalFlag, ImArray<ImArray<T>> sourceArrays) {
			this.source = sourceArrays;
			if (sourceArrays.Length < 1) {
				this.length = 0;
				this.isNull = false;
			}
			else if (sourceArrays.Length < 2) {
				var firstArray = sourceArrays[0];
				this.length = firstArray.Length;
				this.isNull = firstArray.IsNull;
			}
			else {
				bool hasNotNullArray = false;
				var sumLength = 0;
				var srcLength = sourceArrays.Length;
				for (var i = 0; i < srcLength; i++) {
					var array = sourceArrays[i];
					if (array.IsNull) { continue; }

					sumLength += array.Length;
					hasNotNullArray = true;
				}

				this.length = sumLength;
				this.isNull = !hasNotNullArray;
			}
		}

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="arrays">Arrays which will be used for final immutable array.</param>
		public ConcatedArray(IEnumerable<ImArray<T>> arrays) : this(default(InternalFlag), arrays.Where(InternalHelper.IsNotNull).ToImArray()) { }
		#endregion Constructors

		/// <inheritdoc />
		public override IEnumerator<T> GetEnumerator() => new ConcatedArrayEnumerator(this);
	}
}
