﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace TcKs.ImmutableArrays {
	/// <summary>
	/// Encapsulates more <see cref="IEnumerable{T}"/> into one <see cref="IEnumerator{T}"/>.
	/// </summary>
	/// <typeparam name="T">Type of elements in enumerables.</typeparam>
	public sealed class ConcatedEnumerator<T> : IEnumerator<T> {
		private IEnumerator<IEnumerable<T>> mainEnumerator;
		private IEnumerator<T> currentEnumerator;

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="enumerables">Enumerables to encapsulate.</param>
		public ConcatedEnumerator(IEnumerator<IEnumerable<T>> enumerables) {
			this.mainEnumerator = enumerables ?? throw new ArgumentNullException(nameof(enumerables));
		}

		/// <inheritdoc />
		public T Current => this.currentEnumerator.Current;
		object IEnumerator.Current => this.Current;

		/// <inheritdoc />
		public bool MoveNext() {
			bool result = false;

			var curr = this.currentEnumerator;
			do {
				while (curr is null) {
					if (!mainEnumerator.MoveNext()) {
						return false;
					}

					this.currentEnumerator = curr = mainEnumerator.Current?.GetEnumerator();
				}

				result = curr.MoveNext();
				if (!result) {
					this.currentEnumerator = curr = null;
				}
			} while (!result);

			return result;
		}

		/// <inheritdoc />
		public void Reset() {
			this.currentEnumerator?.Reset();
			this.mainEnumerator.Reset();
		}

		/// <inheritdoc />
		public void Dispose() {
			this.currentEnumerator?.Dispose();
			this.mainEnumerator.Dispose();
		}
	}
}
