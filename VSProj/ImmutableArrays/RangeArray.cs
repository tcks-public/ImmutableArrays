﻿/* Licenced under MIT. Complete license is in LINCENSE.txt in root. */

using System;
using System.Collections.Generic;
using System.Text;

namespace TcKs.ImmutableArrays {
	/// <summary>
	/// The range of immutable array.
	/// </summary>
	/// <typeparam name="T">Type of elements in array.</typeparam>
	public sealed class RangeArray<T> : ImArray<T> {
		#region Source
		private readonly ImArray<T> source;

		/// <summary>
		/// The source array from which this array is constructed.
		/// </summary>
		public ImArray<T> Source => this.source;

		private readonly int startIndex;
		/// <summary>
		/// Start index of subset in source array.
		/// </summary>
		public int StartIndex => this.startIndex;
		#endregion Source

		#region Index members
		/// <inheritdoc />
		public override T this[int index] {
			get {
				if (index < 0) { throw new IndexOutOfRangeException(nameof(index)); }
				if (index >= this.length) { throw new IndexOutOfRangeException(nameof(index)); }

				return this.source[index + this.startIndex];
			}
		}
		#endregion Index members

		#region Nullity, emptiness & length members
		private readonly int length;
		/// <summary>
		/// Length of subset in source array.
		/// </summary>
		public override int Length => this.length;

		/// <inheritdoc />
		public override bool IsNull => this.source.IsNull;
		#endregion Nullity, emptiness & length members

		#region Composition members
		/// <inheritdoc />
		public override bool IsComposed => true;
		#endregion Composition members

		#region Constructors
		/// <summary>
		/// Internal constructor.
		/// </summary>
		/// <param name="checkInput">
		/// The <c>true</c> means the input parameters will be checked for validity.
		/// Othervise none of input parameters will be checked for validity.
		/// </param>
		/// <param name="source">Source array for subset. Can not be null.</param>
		/// <param name="startIndex">Start index of subset in source array.</param>
		/// <param name="length">Length of subset in source array.</param>
		internal RangeArray(bool checkInput, ImArray<T> source, int startIndex, int length) {
			if (checkInput) {
				if (source is null) { throw new ArgumentNullException(nameof(source)); }
				if (startIndex < 0) { throw new ArgumentOutOfRangeException(nameof(startIndex)); }
				if (startIndex > source.Length) { throw new ArgumentOutOfRangeException(nameof(startIndex)); }
				if (length < 0) { throw new ArgumentOutOfRangeException(nameof(length)); }
				if (startIndex + length > source.Length) { throw new ArgumentOutOfRangeException(nameof(length)); }
			}

			this.source = source;
			this.startIndex = startIndex;
			this.length = length;
		}

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="array">Source array for subset. Can not be null.</param>
		/// <param name="startIndex">Start index of subset in source array.</param>
		/// <param name="length">Length of subset in source array.</param>
		public RangeArray(ImArray<T> array, int startIndex, int length) : this(true, array, startIndex, length) { }
		#endregion Constructors
	}
}
