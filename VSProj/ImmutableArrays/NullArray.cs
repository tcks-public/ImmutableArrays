﻿/* Licenced under MIT. Complete license is in LINCENSE.txt in root. */

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace TcKs.ImmutableArrays {
	/// <summary>
	/// Immutable array representing NULL.
	/// </summary>
	/// <remarks>
	/// The <see cref="object.Equals(object)"/> method is overriden and returns <c>true</c> if other object is null or is another instance of <see cref="NullArray{T}"/>.
	/// </remarks>
	/// <typeparam name="T">Type of elements in array.</typeparam>
	public sealed partial class NullArray<T> : ImArray<T> {
		#region Nullity, emptiness & length members
		/// <inheritdoc />
		public override int Length => 0;

		/// <inheritdoc />
		public override bool IsEmpty => true;

		/// <inheritdoc />
		public override bool IsNull => true;

		/// <inheritdoc />
		public override bool IsNullOrEmpty => true;
		#endregion Nullity, emptiness & length members

		#region Index members
		/// <summary>
		/// Always throws <see cref="IndexOutOfRangeException"/> because this array has 0 elements.
		/// </summary>
		/// <param name="index">Index of element to return.</param>
		/// <returns></returns>
		public override T this[int index] => throw new IndexOutOfRangeException();
		#endregion Index members

		#region Composition members
		/// <inheritdoc />
		public override bool IsComposed => false;
		#endregion Composition members

		#region Constructors
		/// <summary>
		/// Constructor.
		/// </summary>
		public NullArray() { }
		#endregion Constructors
	}

	partial class NullArray<T> {
		/// <summary>
		/// Returns <c>true</c> if <paramref name="obj"/> is <c>null</c> or is <see cref="ImArray{T}"/> and <see cref="IsNull"/>.
		/// Othervise returns <c>false</c>.
		/// </summary>
		/// <param name="obj">Object to compare.</param>
		/// <returns></returns>
		public override bool Equals(object obj) => obj is null || (obj is ImArray<T> arr && arr.IsNull);

		/// <summary>
		/// Returns <c>true</c> if <paramref name="other"/> is <see cref="IsNull"/>.
		/// Othervise returns <c>false</c>.
		/// </summary>
		/// <param name="other">Array to compare.</param>
		/// <returns></returns>
		public override bool Equals(ImArray<T> other) => other is null || other.IsNull || base.Equals(other);

		/// <summary>
		/// Returns -1.
		/// </summary>
		/// <returns></returns>
		public override int GetHashCode() => -1;
	}

	partial class NullArray<T> : IEnumerator<T> {
		/// <summary></summary>
		/// <remarks>
		/// This class implements both <see cref="IEnumerable{T}"/> and <see cref="IEnumerator{T}"/>.
		/// Because null array has no element -> enumerator don't need any state -> can be hardcoded -> no other instance of object is needed.
		/// </remarks>
		/// <returns></returns>
		public override IEnumerator<T> GetEnumerator() => this;

		/// <inheritdoc />
		[System.Diagnostics.DebuggerHidden]
		public T Current => throw new IndexOutOfRangeException();

		/// <inheritdoc />
		[System.Diagnostics.DebuggerHidden]
		object IEnumerator.Current => this.Current;

		/// <inheritdoc />
		public bool MoveNext() => false;

		/// <inheritdoc />
		public void Reset() {
			// do nothing
		}

		/// <inheritdoc />
		public void Dispose() {
			// do nothing
		}
	}
}
