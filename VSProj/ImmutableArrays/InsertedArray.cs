﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace TcKs.ImmutableArrays
{
	/// <summary>
	/// Composed immutable array where one or more arrays (<see cref="Inserted"/>) are inserted into <see cref="Source"/> on specified index (<see cref="InsertIndex"/>).
	/// </summary>
	/// <typeparam name="T">Type of elements in array.</typeparam>
    public sealed class InsertedArray<T> : ImArray<T>
    {
		#region Source
		private readonly ImArray<T> source;

		/// <summary>
		/// The base array from which is this array constructed.
		/// </summary>
		public ImArray<T> Source => this.source;

		private readonly int insertIndex;

		/// <summary>
		/// The index where are <see cref="Inserted"/> located.
		/// </summary>
		public int InsertIndex => this.insertIndex;

		private readonly ImArray<ImArray<T>> inserted;

		/// <summary>
		/// The inserted arrays.
		/// </summary>
		public ImArray<ImArray<T>> Inserted => this.inserted;

		private readonly int insertedLength;

		/// <summary>
		/// The total lenght of <see cref="Inserted"/>.
		/// </summary>
		public int InsertedLength => this.insertedLength;
		#endregion Source

		#region Nullity, emptiness & length members
		private readonly int length;
		/// <inheritdoc />
		public override int Length => this.length;

		private readonly bool isNull;
		/// <inheritdoc />
		public override bool IsNull => this.isNull;

		/// <inheritdoc />
		public override bool IsNullOrEmpty => false;
		#endregion Nullity, emptiness & length members

		#region Index members
		/// <inheritdoc />
		public override T this[int index] {
			get {
				if (index < 0 || index >= length) { throw new IndexOutOfRangeException(); }

				var insIndex = this.insertIndex;
				var insLength = this.insertedLength;
				var finalIndex = index;
				if (index >= insIndex) {
					var tmpIndex = index - insIndex;

					if (tmpIndex >= insLength) {
						finalIndex -= insLength;
					}
					else {
						return InternalHelper.GetElementAt(this.inserted, tmpIndex);
					}
				}

				return this.source[finalIndex];
			}
		}
		#endregion Index members

		#region Composition members
		/// <inheritdoc />
		public override bool IsComposed => true;
		#endregion Composition members

		#region Constructors
		/// <summary>
		/// Internal constructor.
		/// </summary>
		/// <param name="internalFlag">Value is ignored. This parameter is used to distinction between internal and public constructor.</param>
		/// <param name="source">Base array.</param>
		/// <param name="index">Index where to insert.</param>
		/// <param name="insertedArrays">Inserting arrays.</param>
		internal InsertedArray(InternalFlag internalFlag, ImArray<T> source, int index, ImArray<ImArray<T>> insertedArrays) {
			if (index < 0) { throw new ArgumentOutOfRangeException(nameof(index)); }

			var sourceLength = source.Length;
			if (index > sourceLength) { throw new ArgumentOutOfRangeException(nameof(index)); }

			this.source = source;
			this.insertIndex = index;
			this.inserted = insertedArrays;

			var insLength = InternalHelper.GetLength(insertedArrays);
			this.insertedLength = insLength;
			this.length = sourceLength + insLength;

			this.isNull = source.IsNull && insertedArrays.IsNull;
		}

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="source">Base array.</param>
		/// <param name="index">Index where to insert.</param>
		/// <param name="insertedArrays">Inserting arrays.</param>
		public InsertedArray(ImArray<T> source, int index, IEnumerable<ImArray<T>> insertedArrays)
			: this(default(InternalFlag),
					source ?? throw new ArgumentNullException(nameof(source)),
					index,
					insertedArrays.Where(InternalHelper.IsNotNull).ToImArray())
		{
		}
		#endregion Constructors
	}
}
